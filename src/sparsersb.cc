/*
 Copyright (C) 2011-2021   Michele Martone   <michelemartone _AT_ users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

/*
 * This obsolete wishlist. Patches are welcome!:
 * adapt to when octave_idx_type is 64 bit long
 * rsb_file_vec_save (1.1)
 * all *.m files shall go to inst/
 * switch to using bootstrap.sh (instead autogen.sh) and configure.ac with environment variables, so it can be called from pkg install sparsersb-1.0.4.tar.gz
 * produce ../doc/sparsersb.txi; can use get_help_text
 * put to ./devel/ what is not to be distributed
 * make or configure should fail on missing library (actually it does not)
 * spfind in order to update easily and quickly nonzeroes
 * need A(IA,JA,VA)=nVA
 * shall add "load"; implicit filename based is confusing
 * shall rename "load"/"save" to "loadMatrixMarket"/... or something explicit
 * save/load capability (in own, rsb format)
 * should not rely on string_value().c_str()  --- stack corruption danger!
 * ("get","RSB_IO_WANT_...") is not yet available
 * (.) is incomplete. it is needed by trace()
 * (:,:) , (:,p): test with octave's bicg, bicgstab, cgs, ...
 * hints about how to influence caching blocking policy
 * compound_binary_op
 * for thorough testing, see Octave's test/build_sparse_tests.sh
 * sparsersb(rsbmat,"benchmark")
 * sparsersb(rsbmat,"test")
 * minimize data copies
 * subsref, dotref, subsasgn are incomplete: need error messages there
 * in full_value(), bool arg is ignored
 * symmetry support is incomplete (scarcely defined)
 * document semantics of update and access operators
 * define more operators (e.g.: scaling) for 'complex'
 * create a single error macro for constructors
 * often missing array lenghts/type checks
 * may define as map (see is_map) so that "a.type = ..." can work
 * is_struct, find_nonzero_elem_idx  are undefined
 * are octave_triangular_conv, default_numeric_conversion_function ok ?
 * error reporting is insufficient
 * update to symmetric be forbidden or rather trigger a conversion ?
 * after file read, return various structural info
 * norm computation
 * reformat code for readability
 * warnings about incomplete complex implementation may be overzealous.
 * need matrix exponentiation through conversion to octave format.
 * Note: although librsb has been optimized for performance, sparsersb is not.
 * Note: there are dangerous casts to rsb_coo_idx_t in subsasgn: for 64-bit octave_idx_type.
 * adopt a higher C++ level when possible

 * Developer notes:
 http://www.gnu.org/software/octave/doc/interpreter/index.html
 http://www.gnu.org/software/octave/doc/interpreter/Oct_002dFiles.html#Oct_002dFiles
 http://octave.sourceforge.net/developers.html
 */

#define RSBOI_WANT_PRINT_PCT_OCTAVE_STYLE 1

#include <octave/oct.h>
#define RSBOI_USE_PATCH_OCT44 (OCTAVE_MAJOR_VERSION>=5) || ( (OCTAVE_MAJOR_VERSION==4) && (OCTAVE_MINOR_VERSION>=4))
#if RSBOI_USE_PATCH_OCT44
#include <octave/variables.h>
#include <octave/interpreter.h>
#include <octave/mach-info.h>
#endif /* RSBOI_USE_PATCH_OCT44 */
#include <octave/ov-re-mat.h>
#include <octave/ov-re-sparse.h>
#include <octave/ov-bool-sparse.h> /* RSBOI_WANT_SPMTX_SUBSREF || RSBOI_WANT_SPMTX_SUBSASGN */
#include <octave/ov-scalar.h>
#include <octave/ov-complex.h>
#include <octave/ops.h>
#include <octave/ov-typeinfo.h>
#if RSBOI_WANT_PRINT_PCT_OCTAVE_STYLE
#include <iomanip>	// std::setprecision
#endif
#include <rsb.h>

#if RSBOI_USE_PATCH_OCT44
/* transitional macros, new style */
#define RSBOI_TRY_BLK try
#define RSBOI_CATCH_BLK catch (octave::execution_exception& e) { goto err; }
#define RSBOI_IF_ERR(STMT)
#define RSBOI_IF_NERR(STMT) STMT
#define RSBOI_IF_NERR_STATE()
/* transitional macros, old style */
#else /* RSBOI_USE_PATCH_OCT44 */
#define RSBOI_IF_ERR(STMT)  if (  error_state) STMT
#define RSBOI_IF_NERR(STMT) if (! error_state) STMT
#define RSBOI_IF_NERR_STATE() if (! error_state)
#endif /* RSBOI_USE_PATCH_OCT44 */

//#define RSBOI_VERBOSE_CONFIG 1 /* poor man's trace facility */
#ifdef RSBOI_VERBOSE_CONFIG /* poor man's trace facility */
#if (RSBOI_VERBOSE_CONFIG>0)
#define RSBOI_VERBOSE RSBOI_VERBOSE_CONFIG
#endif
#endif

#define RSBOI_USE_PATCH_38143 ( defined(OCTAVE_MAJOR_VERSION) && (OCTAVE_MAJOR_VERSION>=4) ) /* See http://savannah.gnu.org/bugs/?48335#comment5 */

#if 0
#define RSBOI_WARN( MSG ) \
	octave_stdout << "Warning in "<<__func__<<"(), in file "<<__FILE__<<" at line "<<__LINE__<<":\n" << MSG;
#define RSBOI_FIXME( MSG ) RSBOI_WARN( MSG )/* new */
#else
#define RSBOI_WARN( MSG )
#endif
#define RSBOI_TODO( MSG ) RSBOI_WARN( MSG )/* new */
#define RSBOI_FIXME( MSG ) RSBOI_WARN( "FIXME: "MSG )/* new */

#define RSBOI_PRINTF( ... ) printf( __VA_ARGS__ )
#if RSBOI_VERBOSE
//printf("In file %20s (in %s) at line %10d:\n",__FILE__,__func__,__LINE__),
#define RSBOI_DEBUG_NOTICE( ... ) \
	printf("In %s(), in file %s at line %10d:\n",__func__,__FILE__,__LINE__), \
	printf( __VA_ARGS__ )
#if 0
#define RSBOI_ERROR( ... ) \
	printf("In %s(), in file %s at line %10d:\n",__func__,__FILE__,__LINE__), \
	printf( __VA_ARGS__ )
#else
#define RSBOI_ERROR( MSG ) \
	octave_stdout << "In "<<__func__<<"(), in file "<<__FILE__<<" at line "<<__LINE__<<":\n"<<MSG
#endif
#define RSBOI_DUMP RSBOI_PRINTF
#else
#define RSBOI_DUMP( ... )
#define RSBOI_DEBUG_NOTICE( ... )
#define RSBOI_ERROR( ... )
#endif
#define RSBOI_EERROR( MSG ) \
	octave_stdout << "In "<<__func__<<"(), in file "<<__FILE__<<" at line "<<__LINE__<<":\n"
#define RSBOI_TYPECODE RSB_NUMERICAL_TYPE_DOUBLE
#define RSBOI_RB RSB_DEFAULT_ROW_BLOCKING
#define RSBOI_CB RSB_DEFAULT_COL_BLOCKING
//#define RSBOI_RF RSB_FLAG_DEFAULT_STORAGE_FLAGS
#define RSBOI_RF RSB_FLAG_DEFAULT_RSB_MATRIX_FLAGS
#define RSBOI_DCF RSB_FLAG_DUPLICATES_SUM
#define RSBOI_NF RSB_FLAG_NOFLAGS
//#define RSBOI_EXPF RSB_FLAG_NOFLAGS
#define RSBOI_EXPF RSB_FLAG_IDENTICAL_FLAGS
#define RSBOI_T double
#undef RSB_FULLY_IMPLEMENTED
#define RSBOI_DESTROY(OM) {rsb_mtx_free(OM);(OM)=RSBOI_NULL;}
#define RSBOI_SOME_ERROR(ERRVAL) (ERRVAL)!=RSB_ERR_NO_ERROR
#define RSBOI_0_ERROR error
#define RSBOI_0_BADINVOERRMSG "invoking this function in the wrong way!\n"
#define RSBOI_0_ALLERRMSG "error allocating matrix!\n"
#define RSBOI_0_NOCOERRMSG "compiled without complex type support!\n"
#define RSBOI_0_NOTERRMSG "matrix is not triangular!\n"
#define RSBOI_0_ICSERRMSG "compiled with incomplete complex type support!\n"
#define RSBOI_0_EMERRMSG  "data structure is corrupt (unexpected NULL matrix pointer)!\n"
#define RSBOI_0_ZODERRMG  "are you trying to backsolve a matrix with zeros on the diagonal?\n" // sparsersb([0,0,0;1,0,0;1,1,1])\[1;1;1]
#define RSBOI_0_UNFFEMSG  "unfinished feature\n"
#define RSBOI_0_INCFERRMSG "incomplete function!\n"
#define RSBOI_0_INMISMMSG  "Index sizes of Octave differs from that of RSB:" " a conversion is needed, but yet unsupported in this version."
#define RSBOI_0_UNCFEMSG  "complex support is yet incomplete\n"
#define RSBOI_0_NEEDERR "an error condition needs to be handled, here!\n"
#define RSBOI_0_UNCBERR "matrix NOT correctly built!\n"
#define RSBOI_0_ALERRMSG  "error allocating an rsb matrix!\n"
#define RSBOI_0_WTRANSMSG "invalid transposition character!\n"
#define RSBOI_0_FATALNBMSG  "fatal error! matrix NOT built!\n"
#define RSBOI_0_ASSERRMSG  "assignment is still unsupported on 'sparse_rsb' matrices"
#define RSBOI_0_NSQERRMSG  "matrix is not square"
#define RSBOI_0_NIYERRMSG  "not implemented yet in sparsersb"
#define RSBOI_0_INTERRMSG  "internal sparsersb error: this might be a bug -- please contact and tell us about this!"
#define RSBOI_0_INTERRMSGSTMT(STMT)  {error ("%s", RSBOI_0_INTERRMSG);STMT;}
//#define RSBOI_0_INTERRMSGSTMT(STMT)                           STMT;
#define RSBOI_D_EMPTY_MSG  ""
#define RSBOI_O_MISSIMPERRMSG  "implementation missing here\n"
#define RSBOI_O_NPMSERR  "providing non positive matrix size is not allowed!"
#define RSBOI_0_EMCHECK(M) if(!(M))RSBOI_0_ERROR(RSBOI_0_EMERRMSG);
#define RSBOI_FNSS(S)	#S
#ifndef RSB_SPARSERSB_LABEL
#define RSB_SPARSERSB_LABEL sparsersb
#endif /* RSB_SPARSERSB_LABEL */
//#define RSBOI_FNS	RSBOI_FNSS(RSB_SPARSERSB_LABEL)
#define RSBOI_FSTR	"Recursive Sparse Blocks"
#define RSBOI_FNS	"sparsersb"
#define RSBOI_LIS	"?"

#define RSBIO_DEFAULT_CORE_MATRIX  Matrix (0,0)
/* FIXME : octave_idx_type vs rsb_coo_idx_t */
#define RSBIO_NULL_STATEMENT_FOR_COMPILER_HAPPINESS {(void)1;}
#define RSBOI_OV_STRIDE 1
#define RSBOI_ZERO 0.0
//#define RSB_OI_DMTXORDER RSB_FLAG_WANT_ROW_MAJOR_ORDER
#define RSB_OI_DMTXORDER RSB_FLAG_WANT_COLUMN_MAJOR_ORDER  /* for dense matrices (multivectors) */
#define RSB_OI_TYPEINFO_STRING "rsb sparse matrix"
#define RSB_OI_TYPEINFO_TYPE    "double"

#ifdef RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX
#define RSBOI_WANT_DOUBLE_COMPLEX 1
#define ORSB_RSB_TYPE_FLAG(OBJ) (((OBJ).iscomplex())?RSB_NUMERICAL_TYPE_DOUBLE:RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX)
#else
#define RSBOI_WANT_DOUBLE_COMPLEX 0
#define ORSB_RSB_TYPE_FLAG(OBJ) RSB_NUMERICAL_TYPE_DOUBLE
#endif

#define RSBOI_USE_CXX11 ( defined(__cplusplus) && (__cplusplus>=201103L) )
#if defined(RSBOI_USE_CXX11)
#define RSBOI_NULL nullptr
#else /* RSBOI_USE_CXX11 */
#define RSBOI_NULL NULL
#endif /* RSBOI_USE_CXX11 */
#define RSBOI_INFOBUF	256
#define RSBOI_WANT_SYMMETRY 1
#define RSBOI_WANT_PRINT_DETAIL 0
#define RSBOI_WANT_PRINT_COMPLEX_OR_REAL 0
#define RSBOI_WANT_SUBSREF 1
#define RSBOI_WANT_HEAVY_DEBUG 0
#define RSBOI_WANT_VECLOAD_INSTEAD_MTX 1
#define RSBOI_WANT_MTX_LOAD 1
#define RSBOI_WANT_MTX_SAVE 1
#define RSBOI_WANT_POW 1
#define RSBOI_WANT_QSI 1 /* query string interface */
#define RSBOI_WANT_RESHAPE 1
#define RSBOI_WANT_SPMTX_SUBSREF 0 /* not yet there: need to accumulate in sparse */
#define RSBOI_WANT_SPMTX_SUBSASGN 1
#define RSBOI_WANT_OS_1D_IDX_ACCESS 1 /* Octave-style 1D index access */
#define RSBOI_WANT_EXPAND_SYMM 1 /* Expand symmetry when converting to sparse */
//#define RSBOI_PERROR(E) rsb_perror(E)
#define RSBOI_PERROR(E) if(RSBOI_SOME_ERROR(E)) rsboi_strerr(E)
#ifdef RSB_TRANSPOSITION_INVALID
#define RSBOI_INVALID_TRANS_CHAR RSB_TRANSPOSITION_INVALID /* since librsb-1.2.0.10 */
#else /* RSB_TRANSPOSITION_INVALID */
#define RSBOI_INVALID_TRANS_CHAR '?'
#endif /* RSB_TRANSPOSITION_INVALID */
#ifdef RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX
#include <octave/ov-cx-mat.h>
#include <octave/ov-cx-sparse.h>
#endif

#ifndef RSBOI_RSB_MATRIX_SOLVE
#define RSBOI_RSB_MATRIX_SOLVE(V1,V2) RSBOI_0_ERROR(RSBOI_0_NOTERRMSG)  /* any solution routine shall attached here */
#endif

#if 1
extern "C" { rsb_err_t rsb_dump_postscript_from_mtx_t(const struct rsb_mtx_t *mtxAp, rsb_blk_idx_t br, rsb_blk_idx_t bc, int width, int height, rsb_bool_t all_nnz); }
extern "C" {
rsb_err_t rsb_dump_postscript_recursion_from_mtx_t(const struct rsb_mtx_t *mtxAp, rsb_blk_idx_t br, rsb_blk_idx_t bc, int width, int height, rsb_flags_t flags, rsb_bool_t want_blocks, rsb_bool_t z_dump , rsb_bool_t want_nonzeros ); }
#endif

#if RSBOI_WANT_HEAVY_DEBUG
extern "C" {
	rsb_bool_t rsb_is_correctly_built_rcsr_matrix(const struct rsb_mtx_t *mtxAp); // forward declaration
}
#endif
#if !RSBOI_WANT_OS_1D_IDX_ACCESS
#if defined(RSB_LIBRSB_VER) && (RSB_LIBRSB_VER>=10100)
extern "C" {
#if (RSB_LIBRSB_VER<=10200)
	int rsb_do_get_nnz_element(struct rsb_mtx_t *,void*,void*,void*,int);
#elif (RSB_LIBRSB_VER>=10300)
	int rsb__do_get_nnz_element(struct rsb_mtx_t *,void*,void*,void*,int);
 	#define rsb_do_get_nnz_element rsb__do_get_nnz_element
#endif
}
#endif
#endif /* !RSBOI_WANT_OS_1D_IDX_ACCESS */
#if RSBOI_WANT_DOUBLE_COMPLEX
#define RSBOI_BINOP_PREVAILING_TYPE(V1,V2) (((V1).iscomplex()||(V2).iscomplex())?RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX:RSB_NUMERICAL_TYPE_DOUBLE)
#else
#define RSBOI_BINOP_PREVAILING_TYPE(V1,V2) RSBOI_TYPECODE
#endif
#if defined(RSB_LIBRSB_VER) && (RSB_LIBRSB_VER>=10100)
#define RSBOI_10100_DOCH \
"@deftypefnx {Loadable Function}      " RSBOI_FNS " (@var{S},\"render\", @var{FILENAME}[, @var{rWidth}[, @var{rHeight}]])\n"\
"@deftypefnx {Loadable Function} {[@var{O} =]} " RSBOI_FNS " (@var{S},\"autotune\"[, @var{transA}[, @var{NRHS}[, @var{MAXR}[, @var{TMAX}[, @var{TN}[, @var{SF}]]]]]])\n"\

/* #define RSBOI_10100_DOC "If @var{S} is a " RSBOI_FNS " matrix and one of the \"render\",\"renderb\",\"renders\" keywords ... */
#define RSBOI_10100_DOC \
\
"If @var{S} is a @code{" RSBOI_FNS "} matrix and the @code{\"render\"} keyword is specified, and @var{FILENAME} is a string, @var{A} will be rendered as an Encapsulated Postscript file @var{FILENAME}. Optionally, width and height can be specified in @code{@var{rWidth}, @var{rHeight}}. Defaults are 512.\n"\
"\n"\
\
"If @var{S} is a @code{" RSBOI_FNS "} matrix and the @code{\"autotune\"} keyword is specified, autotuning of the matrix will take place, with SpMV and autotuning parameters. Parameters following the @code{\"autotune\"} string are optional. Parameter @var{transA} specifies whether to tune for untransposed (@code{\"N\"}) or transposed (@code{\"T\"}) or conjugated transposed (@code{\"C\"}); @var{NRHS} the number of right hand sides; @var{MAXR} the number of tuning rounds; @var{TMAX} the threads to use. If giving an output argument @var{O}, that will be assigned to the autotuned matrix, and the input one @var{A} will remain unchanged. See librsb documentation for @code{rsb_tune_spmm} to learn more.\n"
#else
#define RSBOI_10100_DOC	""
#define RSBOI_10100_DOCH	""
#endif

#define RSBOI_VERSION	100009	/* e.g. 100009 means 1.0.9 */

#if defined(USE_64_BIT_IDX_T) || defined(OCTAVE_ENABLE_64) || defined(RSBOI_DETECTED_LONG_IDX) /* 4.1.0+ / 4.0.3 / any */
#define RSBOI_O64_R32 1
#else /* USE_64_BIT_IDX_T */
#define RSBOI_O64_R32 0
#endif /* USE_64_BIT_IDX_T */

#define RSBOI_SIZEMAX RSB_MAX_MATRIX_DIM /* Upper limit to librsb matrix dimension. */

static rsb_err_t rsboi_idxv_overflow( const idx_vector & IM, const idx_vector & JM)
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;

	if( IM.extent(0) > RSBOI_SIZEMAX || JM.extent(0) > RSBOI_SIZEMAX )
		errval = RSB_ERR_LIMITS;

	return errval;
}

#if RSBOI_O64_R32
static rsb_err_t rsboi_idx_overflow( rsb_err_t *errvalp, octave_idx_type idx1, octave_idx_type idx2=0, octave_idx_type idx3=0)
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;

	if( idx1 > RSBOI_SIZEMAX || idx2 > RSBOI_SIZEMAX || idx3 > RSBOI_SIZEMAX )
		errval = RSB_ERR_LIMITS;
	if( errvalp )
		*errvalp = errval;

	return errval;
}

static void rsboi_oi2ri( octave_idx_type * IP, rsb_nnz_idx_t nnz)
{
	// octave_idx_type -> rsb_coo_idx_t
	rsb_coo_idx_t * RP = (rsb_coo_idx_t *) IP;

	const octave_idx_type * OP = (const octave_idx_type*) IP;
	rsb_nnz_idx_t nzi;

	for(nzi=0;nzi<nnz;++nzi)
		RP[nzi] = OP[nzi];
}

static void rsboi_ri2oi( rsb_coo_idx_t * IP, rsb_nnz_idx_t nnz)
{
	// rsb_coo_idx_t -> octave_idx_type
	const rsb_coo_idx_t * RP = (const rsb_coo_idx_t *) IP;

	octave_idx_type * OP = (octave_idx_type*) IP;
	rsb_nnz_idx_t nzi;

	for(nzi=0;nzi<nnz;++nzi)
		OP[nnz-(nzi+1)]=RP[nnz-(nzi+1)];
}

static rsb_err_t rsboi_mtx_get_coo(const struct rsb_mtx_t *mtxAp, void * VA, octave_idx_type * IA, octave_idx_type * JA, rsb_flags_t flags )
{
	// assumes tacitly that rsboi_idx_overflow(IA[i],JA[i])==false for i in 0..nnzA-1.
	rsb_err_t errval = RSB_ERR_NO_ERROR;

	errval = rsb_mtx_get_coo(mtxAp, VA, (rsb_coo_idx_t *)IA, (rsb_coo_idx_t*)JA, flags );
	rsb_nnz_idx_t nnzA = 0;
	rsb_mtx_get_info(mtxAp,RSB_MIF_MATRIX_NNZ__TO__RSB_NNZ_INDEX_T,&nnzA); // FIXME: make this a member and use nnz()
	rsboi_ri2oi((rsb_coo_idx_t *)IA,nnzA);
	rsboi_ri2oi((rsb_coo_idx_t *)JA,nnzA);

	return errval;
}

static struct rsb_mtx_t *rsboi_mtx_alloc_from_csc_const(const void *VA, /*const*/ octave_idx_type * IA, /*const*/ octave_idx_type * CP, rsb_nnz_idx_t nnzA, rsb_type_t typecode, rsb_coo_idx_t nrA, rsb_coo_idx_t ncA, rsb_blk_idx_t brA, rsb_blk_idx_t bcA, rsb_flags_t flagsA, rsb_err_t *errvalp)
{
	struct rsb_mtx_t *mtxAp = RSBOI_NULL;

	if( RSBOI_SOME_ERROR(rsboi_idx_overflow(errvalp,nrA,ncA,nnzA) ) )
		goto ret;
	rsboi_oi2ri(IA,nnzA);
	rsboi_oi2ri(CP,ncA+1);
	mtxAp = rsb_mtx_alloc_from_csc_const(VA, (rsb_coo_idx_t *)IA, (rsb_coo_idx_t *)CP, nnzA, typecode, nrA, ncA, brA, bcA, flagsA, errvalp);
	rsboi_ri2oi((rsb_coo_idx_t *)IA,nnzA);
	rsboi_ri2oi((rsb_coo_idx_t *)CP,ncA+1);
ret:
	return mtxAp;
}

static struct rsb_mtx_t *rsboi_mtx_alloc_from_coo_const(const void *VA, /*const*/ octave_idx_type * IA, /*const*/ octave_idx_type * JA, rsb_nnz_idx_t nnzA, rsb_type_t typecode, rsb_coo_idx_t nrA, rsb_coo_idx_t ncA, rsb_blk_idx_t brA, rsb_blk_idx_t bcA, rsb_flags_t flagsA, rsb_err_t *errvalp)
{
	struct rsb_mtx_t *mtxAp = RSBOI_NULL;

	if( RSBOI_SOME_ERROR(rsboi_idx_overflow(errvalp,nrA,ncA,nnzA) ) )
		goto ret;
	rsboi_oi2ri(IA,nnzA);
	rsboi_oi2ri(JA,nnzA);
	mtxAp = rsb_mtx_alloc_from_coo_const(VA, (rsb_coo_idx_t *)IA, (rsb_coo_idx_t *)JA, nnzA, typecode, nrA, ncA, brA, bcA, flagsA, errvalp);
	rsboi_ri2oi((rsb_coo_idx_t *)IA,nnzA);
	rsboi_ri2oi((rsb_coo_idx_t *)JA,nnzA);
ret:
	return mtxAp;
}
#else /* RSBOI_O64_R32 */
static rsb_err_t rsboi_mtx_get_coo(const struct rsb_mtx_t *mtxAp, void * VA, octave_idx_type * IA, octave_idx_type * JA, rsb_flags_t flags )
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	errval = rsb_mtx_get_coo(mtxAp, VA, IA, JA, flags );
	return errval;
}

static struct rsb_mtx_t *rsboi_mtx_alloc_from_csc_const(const void *VA, const octave_idx_type * IA, const octave_idx_type * CP, rsb_nnz_idx_t nnzA, rsb_type_t typecode, rsb_coo_idx_t nrA, rsb_coo_idx_t ncA, rsb_blk_idx_t brA, rsb_blk_idx_t bcA, rsb_flags_t flagsA, rsb_err_t *errvalp)
{
	struct rsb_mtx_t *mtxAp = RSBOI_NULL;
	mtxAp = rsb_mtx_alloc_from_csc_const(VA, IA, CP, nnzA, typecode, nrA, ncA, brA, bcA, flagsA, errvalp);
	return mtxAp;
}

static struct rsb_mtx_t *rsboi_mtx_alloc_from_coo_const(const void *VA, const octave_idx_type * IA, const octave_idx_type * JA, rsb_nnz_idx_t nnzA, rsb_type_t typecode, rsb_coo_idx_t nrA, rsb_coo_idx_t ncA, rsb_blk_idx_t brA, rsb_blk_idx_t bcA, rsb_flags_t flagsA, rsb_err_t *errvalp)
{
	struct rsb_mtx_t *mtxAp = RSBOI_NULL;
	mtxAp = rsb_mtx_alloc_from_coo_const(VA, IA, JA, nnzA, typecode, nrA, ncA, brA, bcA, flagsA, errvalp);
	return mtxAp;
}
#endif /* RSBOI_O64_R32 */

static std::string rsboi_errstr(rsb_err_t errval)
{
	const int errstrlen = 128;
	char errstr[errstrlen];

	rsb_strerror_r(errval,errstr,errstrlen);
	return errstr;
}

static void rsboi_strerr(rsb_err_t errval)
{
	const std::string errstr = rsboi_errstr(errval);

	octave_stdout<<"librsb error:"<<errstr<<"\n";
}

static void rsboi_error(rsb_err_t errval)
{
	// may invoke this from RSBOI_ERROR
	if(RSBOI_SOME_ERROR(errval))
	{
		const std::string errstr = "librsb error: " + rsboi_errstr(errval);
		error ( "%s", errstr.c_str() );
	}
}

static void RSBOI_ERRMSG(const std::string & errmsg)
{
	octave_stdout << "sparsersb error: " << errmsg << "\n";
}

struct rsboi_coo_matrix_t		//** helper struct with non-owning pointers */
{
	octave_idx_type * IA, * JA;	 /** row and columns indices */
	octave_idx_type nrA,ncA;	 /** matrix rows, columns */
	octave_idx_type nnzA;		 /** matrix (declared) nonzeros */
	RSBOI_T * VA;			 /** values of data elements */
	rsb_type_t typecode;		 /** as specified in the RSB_NUMERICAL_TYPE_* preprocessor symbols in types.h 	*/
};

static const RSBOI_T rsboi_pone[] = {+1.0,0.0};
static const RSBOI_T rsboi_mone[] = {-1.0,0.0};
static const RSBOI_T rsboi_zero[] = { 0.0,0.0}; /* two elements, as shall work also with complex */

static octave_base_value *default_numeric_conversion_function (const octave_base_value& a);

static bool sparsersb_tester(void)
{
#if (RSBOI_VERSION < 100002)
	if(sizeof(octave_idx_type)!=sizeof(rsb_coo_idx_t))
	{
		RSBOI_ERRMSG(RSBOI_0_INMISMMSG);
		goto err;
	}
#else /* RSBOI_VERSION */
	if(sizeof(octave_idx_type)< sizeof(rsb_coo_idx_t))
	{
		RSBOI_ERRMSG(RSBOI_0_INMISMMSG);
		goto err;
	}
#endif /* RSBOI_VERSION */
	RSBOI_WARN(RSBOI_0_INCFERRMSG);
	return true;
err:
	return false;
}

static bool rsboi_sparse_rsb_loaded = false;

class octave_sparsersb_mtx : public octave_sparse_matrix
{
	private:
	public:
	struct rsb_mtx_t *mtxAp;
	public:
		octave_sparsersb_mtx (void) : octave_sparse_matrix(RSBIO_DEFAULT_CORE_MATRIX)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			this->mtxAp = RSBOI_NULL;
		}

		octave_sparsersb_mtx (const octave_sparse_matrix &sm) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		}

#if RSBOI_WANT_MTX_LOAD
		octave_sparsersb_mtx (const std::string &mtxfilename, rsb_type_t typecode = RSBOI_TYPECODE) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX)
		{
			rsb_err_t errval = RSB_ERR_NO_ERROR;

			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			if(!(this->mtxAp = rsb_file_mtx_load(mtxfilename.c_str(),RSBOI_RF,typecode,&errval)))
#if RSBOI_WANT_VECLOAD_INSTEAD_MTX
				/* no problem */;
#else
				RSBOI_ERRMSG(RSBOI_0_ALERRMSG);
			RSBOI_PERROR(errval);
			if(!this->mtxAp)
				RSBOI_0_ERROR(RSBOI_0_ALLERRMSG);
#endif
		}
#endif

		//void alloc_rsb_mtx_from_coo_copy(const idx_vector &IM, const idx_vector &JM, const void * SMp, octave_idx_type nrA, octave_idx_type ncA, bool iscomplex=false, rsb_flags_t eflags=RSBOI_DCF)
		void alloc_rsb_mtx_from_coo_copy(idx_vector & IM, idx_vector & JM, const void * SMp, octave_idx_type nrA, octave_idx_type ncA, bool iscomplex=false, rsb_flags_t eflags=RSBOI_DCF)
		{
			const octave_idx_type nnzA = IM.length();
			rsb_err_t errval = RSB_ERR_NO_ERROR;
#if RSBOI_WANT_DOUBLE_COMPLEX
			const rsb_type_t typecode = iscomplex?RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX:RSB_NUMERICAL_TYPE_DOUBLE;
#else /* RSBOI_WANT_DOUBLE_COMPLEX */
			const rsb_type_t typecode = RSBOI_TYPECODE;
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
			const octave_idx_type *IA = RSBOI_NULL,*JA = RSBOI_NULL;
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
#if RSBOI_WANT_SYMMETRY
			/* shall verify if any symmetry is present */
#endif

			IA = (const octave_idx_type*)IM.raw();
		       	JA = (const octave_idx_type*)JM.raw();

			//RSB_DO_FLAG_ADD(eflags,rsb_util_determine_uplo_flags(IA,JA,nnzA));

			if( (nrA==0 || ncA==0) && RSBOI_SOME_ERROR(errval=rsboi_idxv_overflow( IM, JM )))
				goto err;

			if(!(this->mtxAp = rsboi_mtx_alloc_from_coo_const(SMp,(octave_idx_type*)IA,(octave_idx_type*)JA,nnzA,typecode,nrA,ncA,RSBOI_RB,RSBOI_CB,RSBOI_RF|eflags ,&errval)))
				RSBOI_ERRMSG(RSBOI_0_ALERRMSG);
err:
			RSBOI_PERROR(errval);
			if(!this->mtxAp)
				RSBOI_0_ERROR(RSBOI_0_ALLERRMSG);
		}

#if RSBOI_WANT_DOUBLE_COMPLEX
		octave_sparsersb_mtx (idx_vector &IM, idx_vector &JM, const ComplexMatrix &SM,
			octave_idx_type nrA, octave_idx_type ncA, rsb_flags_t eflags) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			this->alloc_rsb_mtx_from_coo_copy(IM,JM,SM.data(),nrA,ncA,true,eflags);
		}
#endif

		octave_sparsersb_mtx (idx_vector &IM, idx_vector &JM, const Matrix &SM,
			octave_idx_type nrA, octave_idx_type ncA, rsb_flags_t eflags) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			this->alloc_rsb_mtx_from_coo_copy(IM,JM,SM.data(),nrA,ncA,false,eflags);
		}

		void alloc_rsb_mtx_from_csc_copy(const SparseMatrix &sm)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			const octave_idx_type nrA = sm.rows ();
		       	const octave_idx_type ncA = sm.cols ();
			const rsb_nnz_idx_t nnzA = sm.nnz();
			rsb_err_t errval = RSB_ERR_NO_ERROR;
			rsb_flags_t eflags = RSBOI_RF;
			const rsb_type_t typecode = RSB_NUMERICAL_TYPE_DOUBLE;

#if RSBOI_WANT_SYMMETRY
			if(sm.issymmetric())
				RSB_DO_FLAG_ADD(eflags,RSB_FLAG_LOWER_SYMMETRIC|RSB_FLAG_TRIANGULAR);
			// It would be wise to have an isdiag() check and remove symmetry in that case.
#endif
			if(!(this->mtxAp = rsboi_mtx_alloc_from_csc_const(sm.data(),sm.ridx(),sm.cidx(), nnzA,typecode, nrA, ncA, RSBOI_RB, RSBOI_CB, eflags,&errval)))
				RSBOI_ERRMSG(RSBOI_0_ALLERRMSG);
			RSBOI_PERROR(errval);
			if(!this->mtxAp)
				RSBOI_0_ERROR(RSBOI_0_ALLERRMSG);
		}

		octave_sparsersb_mtx (const Matrix &m) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			SparseMatrix sm(m);
			this->alloc_rsb_mtx_from_csc_copy(sm);
		}

#if RSBOI_WANT_DOUBLE_COMPLEX
		void alloc_rsb_mtx_from_csc_copy(const SparseComplexMatrix &sm)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			const octave_idx_type nrA = sm.rows ();
			const octave_idx_type ncA = sm.cols ();
			const octave_idx_type nnzA = sm.nnz ();
			rsb_err_t errval = RSB_ERR_NO_ERROR;
			rsb_flags_t eflags = RSBOI_RF;
			const rsb_type_t typecode = RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX;

#if RSBOI_WANT_SYMMETRY
			if(sm.ishermitian())
			{
				// It would be nice to have SparseComplexMatrix::isdiag() or SparseComplexMatrix::issymmetric() at our disposal here..
				bool is_sym = false;

				if(sm.issquare() && sm.nnz() <= sm.rows())
				{
					SparseComplexMatrix d = sm.diag();
					if(!d.all_elements_are_real() && d.nnz() == sm.nnz())
						is_sym = true; // diagoanl matrix with non-real on diagonal
				}
				if(is_sym)
					RSB_DO_FLAG_ADD(eflags,RSB_FLAG_LOWER_SYMMETRIC|RSB_FLAG_TRIANGULAR);
				else
					RSB_DO_FLAG_ADD(eflags,RSB_FLAG_LOWER_HERMITIAN|RSB_FLAG_TRIANGULAR);
			}
#endif
			if(!(this->mtxAp = rsboi_mtx_alloc_from_csc_const(sm.data(),sm.ridx(),sm.cidx(), nnzA,typecode, nrA, ncA, RSBOI_RB, RSBOI_CB, eflags,&errval)))
				RSBOI_ERRMSG(RSBOI_0_ALLERRMSG);
			RSBOI_PERROR(errval);
			if(!this->mtxAp)
				RSBOI_0_ERROR(RSBOI_0_ALLERRMSG);
		}

		octave_sparsersb_mtx (const ComplexMatrix &cm) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			this->alloc_rsb_mtx_from_csc_copy(SparseComplexMatrix(cm));
		}

		octave_sparsersb_mtx (const SparseComplexMatrix &sm, rsb_type_t typecode = RSBOI_TYPECODE) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			this->alloc_rsb_mtx_from_csc_copy(sm);
		}
#endif

		octave_sparsersb_mtx (const SparseMatrix &sm, rsb_type_t typecode = RSBOI_TYPECODE) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			this->alloc_rsb_mtx_from_csc_copy(sm);
		}

		octave_sparsersb_mtx (struct rsb_mtx_t *mtxBp) : octave_sparse_matrix (RSBIO_DEFAULT_CORE_MATRIX), mtxAp(mtxBp)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			if(!this->mtxAp)
				RSBOI_0_ERROR(RSBOI_0_ALLERRMSG);
		}

		octave_sparsersb_mtx (const octave_sparsersb_mtx& T) :
		octave_sparse_matrix (T)  {
			rsb_err_t errval = RSB_ERR_NO_ERROR;
			struct rsb_mtx_t *mtxBp = RSBOI_NULL;

		       	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			errval = rsb_mtx_clone(&mtxBp,RSB_NUMERICAL_TYPE_SAME_TYPE,RSB_TRANSPOSITION_N,RSBOI_NULL,T.mtxAp,RSBOI_EXPF);
			RSBOI_PERROR(errval);
			this->mtxAp = mtxBp;
		};
		octave_idx_type length (void) const { return this->nnz(); }
		octave_idx_type nelem (void) const { return this->nnz(); }
		octave_idx_type numel (void) const { RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG); return this->nnz(); }
		octave_idx_type nnz (void) const { rsb_nnz_idx_t nnzA = 0; RSBOI_0_EMCHECK(this->mtxAp); rsb_mtx_get_info(this->mtxAp,RSB_MIF_MATRIX_NNZ__TO__RSB_NNZ_INDEX_T,&nnzA);  return nnzA;}
		dim_vector dims (void) const { RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG); return (dim_vector(this->rows(),this->cols())); }
		octave_idx_type dim1 (void) const { return this->rows(); }
		octave_idx_type dim2 (void) const { return this->cols(); }
		octave_idx_type rows (void) const { rsb_coo_idx_t Anr=0; RSBOI_0_EMCHECK(this->mtxAp); rsb_mtx_get_info(this->mtxAp,RSB_MIF_MATRIX_ROWS__TO__RSB_COO_INDEX_T,&Anr);  return Anr;}
		octave_idx_type cols (void) const { rsb_coo_idx_t Anc=0; RSBOI_0_EMCHECK(this->mtxAp); rsb_mtx_get_info(this->mtxAp,RSB_MIF_MATRIX_COLS__TO__RSB_COO_INDEX_T,&Anc);  return Anc;}
		rsb_flags_t rsbflags(void) const { rsb_flags_t Aflags=0; RSBOI_0_EMCHECK(this->mtxAp); rsb_mtx_get_info(this->mtxAp,RSB_MIF_MATRIX_FLAGS__TO__RSB_FLAGS_T,&Aflags);  return Aflags;}
		rsb_type_t rsbtype(void) const { rsb_type_t Atype=0; RSBOI_0_EMCHECK(this->mtxAp); rsb_mtx_get_info(this->mtxAp,RSB_MIF_MATRIX_TYPECODE__TO__RSB_TYPE_T,&Atype);  return Atype;}
		//octave_idx_type rows (void) const { RSBOI_0_EMCHECK(this->mtxAp);return this->mtxAp->nrA; }
		//octave_idx_type cols (void) const { RSBOI_0_EMCHECK(this->mtxAp);return this->mtxAp->ncA; }
		octave_idx_type columns (void) const { return this->cols(); }
		octave_idx_type nzmax (void) const { return this->nnz(); }
		octave_idx_type capacity (void) const { return this->nnz(); }
		size_t byte_size (void) const { RSBOI_0_EMCHECK(this->mtxAp);size_t so=0;rsb_mtx_get_info(this->mtxAp,RSB_MIF_TOTAL_SIZE__TO__SIZE_T,&so);return so; }

		virtual ~octave_sparsersb_mtx (void)
		{
			RSBOI_DEBUG_NOTICE("destroying librsb matrix %p\n",this->mtxAp);
			RSBOI_DESTROY(this->mtxAp);
		}

		virtual octave_base_value *clone (void) const
		{
			RSBOI_DEBUG_NOTICE("cloning librsb matrix %p\n",this->mtxAp);
			return new octave_sparsersb_mtx (*this);
		}

		virtual octave_base_value *empty_clone (void) const
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			return new octave_sparsersb_mtx ();
		}

		virtual SparseMatrix sparse_matrix_value(bool = false)const
		{
			struct rsboi_coo_matrix_t rcm;
			rsb_err_t errval = RSB_ERR_NO_ERROR;
			rsb_nnz_idx_t nzi;
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			RSBOI_0_EMCHECK(this->mtxAp);
			const rsb_nnz_idx_t nnzA = this->nnz();
#if RSBOI_WANT_EXPAND_SYMM
			const rsb_nnz_idx_t e = (is__symmetric() || is__hermitian()) ? 2 : 1;
		       	rsb_nnz_idx_t nze = 0;
#else
			const rsb_nnz_idx_t e = 1;
		       	const rsb_nnz_idx_t nze = 0;
#endif
			Array<octave_idx_type> IA( dim_vector(1,nnzA*e) );
			Array<octave_idx_type> JA( dim_vector(1,nnzA*e) );
			Array<RSBOI_T> VA( dim_vector(1,nnzA*e) );

			rcm.IA = (octave_idx_type*)IA.data();
			rcm.JA = (octave_idx_type*)JA.data();
			if(!this->is_real_type())
			{
				Array<Complex> VAC( dim_vector(1,nnzA*e) );
				RSBOI_T* VAp = ((RSBOI_T*)VA.data());
				rcm.VA = (RSBOI_T*)VAC.data();

				errval = rsboi_mtx_get_coo(this->mtxAp,rcm.VA,rcm.IA,rcm.JA,RSB_FLAG_C_INDICES_INTERFACE);
				for(nzi=0;nzi<nnzA;++nzi)
					VAp[nzi]=rcm.VA[2*nzi];
			}
			else
			{
				rcm.VA = (RSBOI_T*)VA.data();
				errval = rsboi_mtx_get_coo(this->mtxAp,rcm.VA,rcm.IA,rcm.JA,RSB_FLAG_C_INDICES_INTERFACE);
			}
			RSBOI_PERROR(errval);

#if RSBOI_WANT_EXPAND_SYMM
			if(e==2)
			{
				for(nzi=0;nzi<nnzA;++nzi)
					if ( rcm.IA [nzi] != rcm.JA[nzi] )
					{
						rcm.VA[nnzA+nze]=rcm.VA[nzi];
						rcm.JA [nnzA+nze] = rcm.IA[nzi];
						rcm.IA [nnzA+nze] = rcm.JA[nzi];
						nze++;
					}
			}

			VA.resize1(nnzA + nze);
			IA.resize1(nnzA + nze);
			JA.resize1(nnzA + nze);
#endif
			rcm.nrA = this->rows();
			rcm.ncA = this->cols();

			return SparseMatrix(VA,IA,JA,rcm.nrA,rcm.ncA);
		}

		virtual Matrix matrix_value(bool = false)const
		{
			RSBOI_FIXME("inefficient!");
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			const Matrix cm = this->sparse_matrix_value().matrix_value();
			return cm;
		}

		virtual Matrix full_sym_real_value(void)const
		{
			// Conversion to full, with symmetry expansion.
			RSBOI_FIXME("inefficient (see transpose)!");
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

			const octave_idx_type rn = this->rows(), cn = this->cols();
			Matrix v2(rn,cn,RSBOI_ZERO);
			rsb_err_t errval = RSB_ERR_NO_ERROR;

			errval |= rsb_mtx_add_to_dense(&rsboi_pone,this->mtxAp,rn,rn,cn,RSB_BOOL_TRUE,(RSBOI_T*)v2.data());
			rsboi_error(errval);
			for(int i = 0; i<rn; ++i)
				v2(i,i) = RSBOI_ZERO;
			v2 = v2.transpose();
			errval |= rsb_mtx_add_to_dense(&rsboi_pone,this->mtxAp,rn,rn,cn,RSB_BOOL_TRUE,(RSBOI_T*)v2.data());
			if(RSBOI_SOME_ERROR(errval))
				RSBOI_0_ERROR(RSBOI_0_NOCOERRMSG);
			return v2;
		}

		virtual ComplexMatrix full_sym_cplx_value(void)const
		{
			// Conversion to full, with symmetry expansion.
			RSBOI_FIXME("inefficient (see transpose)!");
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			const octave_idx_type rn = this->rows(), cn = this->cols();
			ComplexMatrix v2(rn,cn,RSBOI_ZERO);
			rsb_err_t errval = RSB_ERR_NO_ERROR;

			errval |= rsb_mtx_add_to_dense(&rsboi_pone,this->mtxAp,rn,rn,cn,RSB_BOOL_TRUE,(RSBOI_T*)v2.data());
			rsboi_error(errval);
			for(int i = 0; i<rn; ++i)
				v2(i,i) = RSBOI_ZERO;
			v2 = v2.transpose();
			errval |= rsb_mtx_add_to_dense(&rsboi_pone,this->mtxAp,rn,rn,cn,RSB_BOOL_TRUE,(RSBOI_T*)v2.data());
			if(RSBOI_SOME_ERROR(errval))
				RSBOI_0_ERROR(RSBOI_0_NOCOERRMSG);
			return v2;
		}

		virtual octave_value full_value(void)const
		{
			RSBOI_FIXME("inefficient!");
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

			if(is__symmetric() || is__hermitian())
			{
				if(this->is_real_type())
					return this->full_sym_real_value();
				else
					return this->full_sym_cplx_value();
			}
			else
			{
				if(this->is_real_type())
					return this->matrix_value();
				else
					return this->complex_matrix_value();
			}
		}

#if RSBOI_WANT_DOUBLE_COMPLEX
		virtual ComplexMatrix complex_matrix_value(bool = false)const
		{
			RSBOI_FIXME("inefficient!");
			const octave_sparse_complex_matrix ocm = this->sparse_complex_matrix_value();
			const ComplexMatrix cm = ocm.complex_matrix_value();
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			return cm;
		}

		virtual SparseComplexMatrix sparse_complex_matrix_value(bool = false)const
		{
			struct rsboi_coo_matrix_t rcm;
			rsb_err_t errval = RSB_ERR_NO_ERROR;
			rsb_nnz_idx_t nzi;
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			RSBOI_0_EMCHECK(this->mtxAp);
			const rsb_nnz_idx_t nnzA = this->nnz();
#if RSBOI_WANT_EXPAND_SYMM
			const rsb_nnz_idx_t e = (is__symmetric() || is__hermitian()) ? 2 : 1;
		       	rsb_nnz_idx_t nze = 0;
#else
			const rsb_nnz_idx_t e = 1;
		       	const rsb_nnz_idx_t nze = 0;
#endif
			Array<octave_idx_type> IA( dim_vector(1,nnzA*e) );
			Array<octave_idx_type> JA( dim_vector(1,nnzA*e) );
			Array<Complex> VA( dim_vector(1,nnzA*e) );
			RSBOI_T* VAp = ((RSBOI_T*)VA.data());

			rcm.IA = (octave_idx_type*)IA.data();
			rcm.JA = (octave_idx_type*)JA.data();
			rcm.VA = VAp;
			errval = rsboi_mtx_get_coo(this->mtxAp,rcm.VA,rcm.IA,rcm.JA,RSB_FLAG_C_INDICES_INTERFACE);
			RSBOI_PERROR(errval);

			if(this->is_real_type())
				for(nzi=0;nzi<nnzA;++nzi)
					VAp[2*(nnzA-1-nzi)+0]=VAp[(nnzA-1-nzi)+0],
					VAp[2*(nnzA-1-nzi)+1]=0;
#if RSBOI_WANT_EXPAND_SYMM
			if(e==2)
			{
				for(nzi=0;nzi<nnzA;++nzi)
					if ( rcm.IA [nzi] != rcm.JA[nzi] )
					{
						if(this->is_real_type())
							rcm.VA[nnzA+nze]=rcm.VA[nzi];
						else
						{
							if(is__hermitian())
								((Complex*)rcm.VA)[nnzA+nze]=conj(((Complex*)rcm.VA)[nzi]);
							else
								((Complex*)rcm.VA)[nnzA+nze]=    (((Complex*)rcm.VA)[nzi]);
						}
						rcm.JA [nnzA+nze] = rcm.IA[nzi];
						rcm.IA [nnzA+nze] = rcm.JA[nzi];
						nze++;
					}
			}

			VA.resize1(nnzA + nze);
			IA.resize1(nnzA + nze);
			JA.resize1(nnzA + nze);
#endif
			rcm.nrA = this->rows();
			rcm.ncA = this->cols();

			return SparseComplexMatrix(VA,IA,JA,rcm.nrA,rcm.ncA);
		}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

		//octave_value::assign_op, int, int, octave_value (&)(const octave_base_value&, const octave_base_value&)
		//octave_value::assign_op, int, int, octave_value (&)
		//octave_value  assign_op (const octave_base_value&, const octave_base_value&) {}
		// octave_value::assign_op octave_value::binary_op_to_assign_op (binary_op op) { assign_op retval; return retval; }

#if RSBOI_WANT_SUBSREF

octave_value do_index_op_subsparse(const idx_vector & i) const
{
	// Convert to Octave's sparse and reconvert.
	// check with
	// octave --eval "nnz(sparse((toeplitz(sparsersb([0,1,2,3]))-toeplitz(sparse([0,1,2,3])))))==0"
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	octave_value retval;

	if(is_real_type())
		retval = new octave_sparsersb_mtx ( SparseMatrix(sparse_matrix_value().index(i)) );
	else
		retval = new octave_sparsersb_mtx ( SparseComplexMatrix(sparse_complex_matrix_value().index(i)) );
	return retval;
}

		octave_value do_index_op(const octave_value_list& idx, bool resize_ok = false)
		{
				RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
				rsb_err_t errval = RSB_ERR_NO_ERROR;
				octave_value retval;

				//if (type.length () == 1)
				{
  					const octave_idx_type n_idx = idx.length ();
					if (n_idx == 0 )
						retval = clone();
					else
					if (n_idx == 1 )
					{
						RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
#if RSBOI_WANT_SPMTX_SUBSREF
						const octave_value_list ovl = idx;
						if(ovl(0).issparse())
						{
  							SparseBoolMatrix sm = SparseBoolMatrix (ovl(0).sparse_matrix_value());
							const octave_idx_type * ir = sm.mex_get_ir ();
							const octave_idx_type * jc = sm.mex_get_jc ();
					        	const octave_idx_type nr = sm.rows ();
        						const octave_idx_type nc = sm.cols ();
							RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

        						for (octave_idx_type j = 0; j < nc; j++)
							{
							  std::cout << jc[j] << ".." << jc[j+1] << "\n";
        						  for (octave_idx_type i = jc[j]; i < jc[j+1]; i++)
							  {
							    std::cout << ir[i] << " " << j << "\n";
							  }
							}
							RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
							retval = octave_value(this->clone()); // matches but .. heavy ?!
						}
						else
#endif /* RSBOI_WANT_SPMTX_SUBSREF */
						{
	    					const idx_vector i = idx (0).index_vector ();
#if   defined(RSB_LIBRSB_VER) && (RSB_LIBRSB_VER< 10100)
						const octave_idx_type ii = i(0);
						RSBOI_ERRMSG("You are using a very old and limited version of librsb: no support for this kind of access\n");
#elif defined(RSB_LIBRSB_VER) && (RSB_LIBRSB_VER>=10100)
#if RSBOI_WANT_RESHAPE
						if( i.is_colon() )
						{
							retval = this->reshape(dim_vector (this->rows()*this->cols(),1));
							goto err;
						}
						if( i.is_range() )
						{
							if(is_real_type())
								retval = new octave_sparsersb_mtx ( SparseMatrix(sparse_matrix_value().index(i)) );
							else
								retval = new octave_sparsersb_mtx ( SparseComplexMatrix(sparse_complex_matrix_value().index(i)) );
							goto err;
						}
#endif /* RSBOI_WANT_RESHAPE */
						RSBOI_DEBUG_NOTICE("i.length () = %d\n",i.length());
						if(i.length()>1)
						{
							retval = do_index_op_subsparse(i);
							goto err;
						}
#if RSBOI_WANT_OS_1D_IDX_ACCESS
						const rsb_coo_idx_t jj = (i(0) / rows());
						const rsb_coo_idx_t ii = (i(0) % rows());
						RSBOI_DEBUG_NOTICE("get_element (%d,%d)\n",ii,ii);
						if(is_real_type())
						{
							RSBOI_T rv;
							//errval = rsb_do_get_nnz_element(this->mtxAp,&rv,RSBOI_NULL,RSBOI_NULL,ii);
       							errval = rsb_mtx_get_values(this->mtxAp,&rv,&ii,&jj,1,RSBOI_NF);
							retval = rv;
						}
						else
						{
							Complex rv { RSBOI_ZERO, RSBOI_ZERO };
							//errval = rsb_do_get_nnz_element(this->mtxAp,&rv,RSBOI_NULL,RSBOI_NULL,ii);
       							errval = rsb_mtx_get_values(this->mtxAp,&rv,&ii,&jj,1,RSBOI_NF);
							retval = rv;
						}
						if(RSBOI_SOME_ERROR(errval))
						{
							if(ii>=this->rows() || ii<0 || jj>=this->cols() || jj<0)
								error ("trying accessing element %ld,%ld: index out of bounds !",(long int)ii+1,(long int)jj+1);
							else
								; /* likely a zero */
						}
#endif /* RSBOI_WANT_OS_1D_IDX_ACCESS */
#endif
						}
					}
					else
					if (n_idx == 2 )
	  				{
					RSBOI_TRY_BLK
					{
	    					const idx_vector i = idx (0).index_vector ();
						RSBOI_IF_NERR_STATE()
	      					{
#if RSBOI_WANT_SYMMETRY
							/* Will expand (:,:) but won't access empty-triangle nonzeros. */
#endif
#if RSBOI_WANT_RESHAPE
							if( idx(0).index_vector ().is_colon() || idx(1).index_vector ().is_colon() )
							{
								if(is_real_type())
								{
									octave_sparse_matrix osm (sparse_matrix_value());
									retval = osm.do_index_op(idx);
								}
								else
								{
									octave_sparse_complex_matrix osm (sparse_complex_matrix_value());
									retval = osm.do_index_op(idx);
								}
								goto err;
							}
#endif /* RSBOI_WANT_RESHAPE */
							if(is_real_type())
							{
								const idx_vector j = idx (1).index_vector ();
								RSBOI_T rv;
						  		rsb_coo_idx_t ii = -1, jj = -1;
  								ii = i(0); jj = j(0);
								RSBOI_DEBUG_NOTICE("get_elements (%d %d)\n",ii,jj);
       								errval = rsb_mtx_get_values(this->mtxAp,&rv,&ii,&jj,1,RSBOI_NF);
								retval = rv;
								RSBOI_IF_NERR(;)
							}
							else
							{
								const idx_vector j = idx (1).index_vector ();
								Complex rv;
						  		rsb_coo_idx_t ii =-1, jj = -1;
  								ii = i(0); jj = j(0);
								RSBOI_DEBUG_NOTICE("get_elements (%d %d) complex\n",ii,jj);
       								errval = rsb_mtx_get_values(this->mtxAp,&rv,&ii,&jj,1,RSBOI_NF);
								retval = rv;
								RSBOI_IF_NERR(;)
							}
	      					}
					}
					RSBOI_CATCH_BLK
	  				}
				}
err:
				return retval;
		}

#if RSBOI_WANT_RESHAPE
		octave_value reshape (const dim_vector& new_dims) const
	       	{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			RSBOI_WARN(RSBOI_0_UNFFEMSG);
			octave_value retval;

			if(is_real_type())
				retval = new octave_sparsersb_mtx ( sparse_matrix_value().reshape(new_dims) );
			else
				retval = new octave_sparsersb_mtx ( sparse_complex_matrix_value().reshape(new_dims) );
			return retval;
	       	}
#endif /* RSBOI_WANT_RESHAPE */

		octave_value subsref (const std::string &type, const std::list<octave_value_list>& idx)
		{
			octave_value retval;
			const int skip = 1;
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

			RSBOI_TRY_BLK
			{
			switch (type[0])
			{
				case '(':
					retval = do_index_op(idx.front());
				break;

				case '.':
					RSBOI_DEBUG_NOTICE("UNFINISHED\n");
					break;

				case '{':
					error ("%s cannot be indexed with %c", type_name().c_str(), type[0]);
					break;

				default:
					panic_impossible ();
			}
			}
			RSBOI_CATCH_BLK
			RSBOI_IF_NERR(
				retval = retval.next_subsref (type, idx, skip);
				)
err:
			return retval;
		} /* subsref */
#else /* RSBOI_WANT_SUBSREF */
		/* FIXME: need an alternative, bogus implementation of subsref */
#endif /* RSBOI_WANT_SUBSREF */

		octave_value_list dotref (const octave_value_list& idx)
		{
			octave_value_list retval;

			const std::string nm = idx(0).string_value ();
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

			/*    if (nm == "type")
				  if (isupper ())
				retval = octave_value ("Upper");
				  else
				retval = octave_value ("Lower");
				else*/
			error ("%s can indexed with .%s",
				type_name().c_str(), nm.c_str());

			return retval;
		}

		bool is_map (void) const { return true; }
		bool issparse(void) const { RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);return true; }
		bool is_real_type (void) const { RSBOI_0_EMCHECK(this->mtxAp); RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);return this->rsbtype()==RSB_NUMERICAL_TYPE_DOUBLE?true:false; }
		bool is_diagonal (void) const { RSBOI_0_EMCHECK(this->mtxAp); RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);return RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_DIAGONAL)?true:false; }/* FIXME: new: not sure whether this is ever called */
		bool is_lower_triangular (void) const { RSBOI_0_EMCHECK(this->mtxAp); RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);return RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_LOWER_TRIANGULAR)?true:false; }/* FIXME: new: not sure whether this is ever called */
		bool is_upper_triangular (void) const { RSBOI_0_EMCHECK(this->mtxAp); RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);return RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_UPPER_TRIANGULAR)?true:false; }/* FIXME: new: not sure whether this is ever called */
		bool iscomplex (void) const { RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG); return !is_real_type(); }
		bool isreal (void) const { RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG); return  is_real_type(); }
		bool is_bool_type (void) const { return false; }
		bool isinteger (void) const { return false; }
		bool is_square (void) const { return this->rows()==this->cols(); }
		bool is_empty (void) const { return false; }
		bool is__symmetric (void) const { if(RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_SYMMETRIC))return true; return false; }
		bool is__hermitian (void) const { if(RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_HERMITIAN))return true; return false; }
		std::string get_symmetry (void) const { return (RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_SYMMETRIC)?"S": (RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_HERMITIAN)?"H":"U")); }
		bool is__triangular (void) const
	       	{
			rsb_bool_t retval = RSB_BOOL_FALSE;
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

		       	if(!this->mtxAp)
			       	retval = RSB_BOOL_FALSE;
			else
#if RSBOI_WANT_SYMMETRY
		       	if( (!RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_SYMMETRIC)) || RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_DIAGONAL) )
#endif
				retval = RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_TRIANGULAR)?RSB_BOOL_TRUE:RSB_BOOL_FALSE;
			return retval;
		}
//		int is_struct (void) const { return false; }

		bool save_ascii (std::ostream& os)
		{
			error ("%s", "save_ascii() " RSBOI_0_NIYERRMSG);
			return false;
		}
		bool load_ascii (std::istream& is)
		{
			error ("%s", "load_ascii() " RSBOI_0_NIYERRMSG);
			return false;
		}
		bool save_binary (std::ostream& os, bool& save_as_floats)
		{
			error ("%s", "save_binary() " RSBOI_0_NIYERRMSG);
			return false;
		}
#if RSBOI_USE_PATCH_OCT44
		bool load_binary (std::istream& is, bool swap, octave::mach_info::float_format fmt)
#else /* RSBOI_USE_PATCH_OCT44 */
		// would break on octave6
		bool load_binary (std::istream& is, bool swap, oct_mach_info::float_format fmt)
#endif /* RSBOI_USE_PATCH_OCT44 */
		{
			error ("%s", "load_binary() " RSBOI_0_NIYERRMSG);
			return false;
		}
		octave_value subsasgn (const std::string& type, const std::list<octave_value_list>& idx, const octave_value& rhs)
		{
			octave_value retval;
#if 0
			rsb_err_t errval = RSB_ERR_NO_ERROR;
#endif
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

			switch (type[0])
			{

				case '(':
				{
				if (type.length () == 1)
				{
					//retval = numeric_assign (type, idx, rhs);
					//RSBOI_DEBUG_NOTICE("UNFINISHED\n");
					const octave_idx_type n_idx = idx.front().length ();
					switch (n_idx)
    					{
						case 0:
						retval = matrix;
						RSBOI_DEBUG_NOTICE("UNFINISHED\n");
						break;
						case 1:
						{
#if RSBOI_WANT_SPMTX_SUBSASGN
					{
						RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
						const octave_value_list ovl = idx.front();
						if(ovl(0).issparse() && ovl(0).isreal() && rhs.isreal())
						{
  							const SparseBoolMatrix sm = SparseBoolMatrix (ovl(0).sparse_matrix_value());
							const octave_idx_type * ir = sm.mex_get_ir ();
							const octave_idx_type * jc = sm.mex_get_jc ();
        						const octave_idx_type nc = sm.cols ();
							RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
							const RSBOI_T rv = rhs.double_value();

        						for (octave_idx_type j = 0; j < nc; j++)
							{
        						  for (octave_idx_type i = jc[j]; i < jc[j+1]; i++)
							  {
							    rsb_err_t errval = RSB_ERR_NO_ERROR;
							    const rsb_coo_idx_t ii = static_cast<rsb_coo_idx_t>(ir[i]); // Note: potentioally dangerous casts, if types are different and matrix huge.
							    const rsb_coo_idx_t jj = static_cast<rsb_coo_idx_t>(j);

							    errval = rsb_mtx_set_values(this->mtxAp,&rv,&ii,&jj,1,RSBOI_NF);
                                                            if(RSBOI_SOME_ERROR(errval))
							      error ("%s", "FIXME: Incomplete: Can only accept already existing indices.");
							  }
							}
							RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
							retval = octave_value(this->clone());
						}
						else
						  error ("%s", "FIXME: Incomplete: no complex sparse-sparse update for the moment.");
					}
#else /* RSBOI_WANT_SPMTX_SUBSASGN */
							RSBOI_DEBUG_NOTICE("UNFINISHED\n");
							idx_vector i = idx.front()(0).index_vector ();
							// ...
							RSBOI_IF_NERR(
								;//retval = octave_value (matrix.index (i, resize_ok));
							)
#endif /* RSBOI_WANT_SPMTX_SUBSASGN */
      						}
						break;
						default:
						{
							if (n_idx == 2 )
							{
								idx_vector i = idx.front() (0).index_vector ();
								idx_vector j = idx.front() (1).index_vector ();
#if 0
								// for op_el_div_eq and op_el_mul_eq
								std :: cout << "ic2 " << i.is_colon() << "\n" ;
								if( i.is_colon() && !j.is_colon() )
								{
									ComplexMatrix cm = rhs.complex_matrix_value();
									std :: cout << " : , .\n";
									errval=rsb_mtx_upd_values(this->mtxAp,RSB_ELOPF_SCALE_ROWS,cm.data());
								}
								if(!i.is_colon() &&  j.is_colon() )
								{
									std :: cout << " . , :\n";
								}
								if( i.is_colon() && j.is_colon() )
								{
									std :: cout << " : , :\n";
								}
#endif
								RSBOI_IF_NERR_STATE()
								{
									if(is_real_type())
									{
										rsb_err_t errval = RSB_ERR_NO_ERROR;
										rsb_coo_idx_t ii = -1, jj = -1;
										RSBOI_T rv = rhs.double_value();
										ii = i(0); jj = j(0);
										RSBOI_DEBUG_NOTICE("update elements (%d %d)\n",ii,jj);
#if RSBOI_WANT_SYMMETRY
										/* FIXME: and now ? */
#endif
										errval = rsb_mtx_set_values(this->mtxAp,&rv,&ii,&jj,1,RSBOI_NF);
										RSBOI_PERROR(errval);
										/* FIXME: I am unsure, here */
										//retval=rhs.double_value(); // this does not match octavej
										//retval=octave_value(this);
										retval = octave_value(this->clone()); // matches but .. heavy ?!
										RSBOI_IF_NERR(
											;//retval = octave_value (matrix.index (i, j, resize_ok));
										)
									}
									else
									{
										rsb_err_t errval = RSB_ERR_NO_ERROR;
										rsb_coo_idx_t ii = -1, jj = -1;
										Complex rv = rhs.complex_value();
										ii = i(0); jj = j(0);
										RSBOI_DEBUG_NOTICE("update elements (%d %d) complex\n",ii,jj);
#if RSBOI_WANT_SYMMETRY
				/* FIXME: and now ? */
#endif
										errval = rsb_mtx_set_values(this->mtxAp,&rv,&ii,&jj,1,RSBOI_NF);
										RSBOI_PERROR(errval);
										/* FIXME: I am unsure, here */
										//retval=rhs.double_value(); // this does not match octavej
										//retval=octave_value(this);
										retval = octave_value(this->clone()); // matches but .. heavy ?!
										RSBOI_IF_NERR(
											;//retval = octave_value (matrix.index (i, j, resize_ok));
										)
									}
//		  class octave_map;
//		  retval = octave_map();
//	RSBOI_DEBUG_NOTICE("UNFINISHED: set %d %d <- %lg\n",ii,jj,rhs.double_value());
	      							}
							}
						}
						break;
					}
					}
					else if (type.length () == 2)
					{
						std::list<octave_value_list>::const_iterator p =
							idx.begin ();
						octave_value_list key_idx = *++p;

						std::string key = key_idx(0).string_value ();
						RSBOI_DEBUG_NOTICE("UNFINISHED\n");

						if (key == "type")
							error ("%s", "use 'sparse_rsb' to set type");
						else
							error ("%s can indexed with .%s",
								type_name().c_str(), key.c_str());
					}
					else
						error ("in indexed assignment of %s, illegal assignment",
							type_name().c_str ());
				}
				break;
				case '.':
				{
					octave_value_list key_idx = idx.front ();
					std::string key = key_idx(0).string_value ();
					RSBOI_DEBUG_NOTICE("UNFINISHED\n");

					if (key == "type")
						error ("%s", "use 'sparse_rsb' to set matrix type");
					else
						error ("%s can indexed with .%s",
							type_name().c_str(), key.c_str());
				}
				break;

				case '{':
					RSBOI_DEBUG_NOTICE("UNFINISHED\n");
					error ("%s cannot be indexed with %c",
						type_name().c_str (), type[0]);
					break;

				default:
					panic_impossible ();
			}
			return retval;
		} /* subsasgn */

		octave_base_value *try_narrowing_conversion (void)
		{
			octave_base_value *retval = 0;
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
			return retval;
		}

		/*
		type_conv_fcn numeric_conversion_function (void) const
		{
		}
		*/

		type_conv_info numeric_conversion_function (void) const
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			return default_numeric_conversion_function;
		}

		std::string get_info_string()
		{
			char ss[RSBOI_INFOBUF];
			rsb_mtx_get_info_str(this->mtxAp,"RSB_MIF_MATRIX_INFO__TO__CHAR_P",ss,RSBOI_INFOBUF);
			return ss;
		}

#if defined(OCTAVE_MAJOR_VERSION) && (OCTAVE_MAJOR_VERSION>=4)
		void print (std::ostream& os, bool pr_as_read_syntax = false)
#else  /* OCTAVE_MAJOR_VERSION */
		void print (std::ostream& os, bool pr_as_read_syntax = false) const
#endif /* OCTAVE_MAJOR_VERSION */
		{
			RSBOI_FIXME("what to do with pr_as_read_syntax ?");
			struct rsboi_coo_matrix_t rcm;
			rsb_err_t errval = RSB_ERR_NO_ERROR;
			rsb_nnz_idx_t nnzA = this->nnz(),nzi;
			bool ic = this->is_real_type()?false:true;
			Array<octave_idx_type> IA( dim_vector(1,nnzA) );
			Array<octave_idx_type> JA( dim_vector(1,nnzA) );
			Array<RSBOI_T> VA( dim_vector(1,(ic?2:1)*nnzA) );
			std::string c = ic ? "complex" : "real";
#if RSBOI_WANT_PRINT_DETAIL
			char ss[RSBOI_INFOBUF];
			rsb_mtx_get_info_str(this->mtxAp,"RSB_MIF_MATRIX_INFO__TO__CHAR_P",ss,RSBOI_INFOBUF);
#endif
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			rcm.VA = (RSBOI_T*)VA.data();
			rcm.IA = (octave_idx_type*)IA.data();
			rcm.JA = (octave_idx_type*)JA.data();
#if RSBOI_WANT_SYMMETRY
			/* No expansion: we merely mention symmetry. */
#endif

			if(! (rcm.VA && rcm.IA && rcm.JA) )
				// NOTE: might rather want an  error ()  here.
				nnzA = 0;
			else
				errval = rsboi_mtx_get_coo(this->mtxAp,rcm.VA,rcm.IA,rcm.JA,RSB_FLAG_C_INDICES_INTERFACE);
			if(RSBOI_SOME_ERROR(errval))
			{
				RSBOI_PERROR(errval);
				return;
			}
			rcm.nrA = this->rows();
			rcm.ncA = this->cols();
			double pct = 100.0*(((RSBOI_T)nnzA)/((RSBOI_T)rcm.nrA))/rcm.ncA;
			octave_stdout<<RSBOI_FSTR<< "  (rows = "<<rcm.nrA<<
				", cols = "<<rcm.ncA<<
				", nnz = "<<nnzA
#if RSBOI_WANT_SYMMETRY
				<< ", symm = "<<
				(RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_SYMMETRIC)?"S":
				(RSB_DO_FLAG_HAS(this->rsbflags(),RSB_FLAG_HERMITIAN)?"H":"U"))
				// NOTE: need a mechanism to print out these flags from rsb itself
#endif
			;
#if RSBOI_WANT_PRINT_PCT_OCTAVE_STYLE
			/* straight from Octave's src/ov-base-sparse.cc */
			if (nnzA > 0)
    			{
      				int prec = 2;
      				if (pct == 100) prec = 3; else { if (pct > 99.9) prec = 4; else if (pct > 99) prec = 3; if (pct > 99.99) pct = 99.99; }
      				octave_stdout << " [" << std::setprecision (prec) << pct << "%]";
    			}
#else
			octave_stdout << " ["<<pct<< "%]";
#endif

			octave_stdout <<
#if RSBOI_WANT_PRINT_COMPLEX_OR_REAL
				", "<<c<<
#endif
				")\n";
#if RSBOI_WANT_PRINT_DETAIL
			octave_stdout<< "{{"<< ss <<"}}\n";
#else
			octave_stdout<< "\n";
#endif
			if(ic)
				for(nzi=0;nzi<nnzA;++nzi)
					octave_stdout<<"  ("<<1+IA(nzi)<<", "<<1+JA(nzi)<<") -> "<<((RSBOI_T*)rcm.VA)[2*nzi+0]<<" + " <<((RSBOI_T*)rcm.VA)[2*nzi+1]<<"i\n";
			else
				for(nzi=0;nzi<nnzA;++nzi)
					octave_stdout<<"  ("<<1+IA(nzi)<<", "<<1+JA(nzi)<<") -> "<<((RSBOI_T*)rcm.VA)[nzi]<<"\n";
			newline(os);
			RSBIO_NULL_STATEMENT_FOR_COMPILER_HAPPINESS
		}

	octave_value diag (octave_idx_type k) const
	{
		octave_value retval;
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		RSBOI_0_EMCHECK(this->mtxAp);

		if(k!=0)
		{
			error ("%s", "only main diagonal extraction is supported !");
		}
		if(this->is_square())
		{
			rsb_err_t errval = RSB_ERR_NO_ERROR;
			//RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			if(this->is_real_type())
			{
				Matrix DA(this->rows(),1);
				errval = rsb_mtx_get_vec(this->mtxAp,(RSBOI_T*)DA.data(),RSB_EXTF_DIAG);
				retval = (DA);
			}
			else
			{
				ComplexMatrix DA(this->rows(),1);
				errval = rsb_mtx_get_vec(this->mtxAp,(RSBOI_T*)DA.data(),RSB_EXTF_DIAG);
				retval = (DA);
			}
			RSBOI_PERROR(errval);
		}
		else
		{
			error ("%s", RSBOI_0_NSQERRMSG);
		}
		return retval;
	}

	octave_value rsboi_get_scaled_copy_inv(const RSBOI_T alpha)const
	{
		const RSBOI_T one = 1.0;
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		return rsboi_get_scaled_copy(one/alpha);
	}

#if RSBOI_WANT_DOUBLE_COMPLEX
	octave_value rsboi_get_scaled_copy_inv(const Complex alpha)const
	{
		const Complex one = 1.0;
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		return rsboi_get_scaled_copy(one/alpha);
	}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

	octave_value rsboi_get_scaled_copy(const RSBOI_T alpha, rsb_trans_t transA=RSB_TRANSPOSITION_N)const
	{
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		struct rsb_mtx_t *mtxBp = RSBOI_NULL;

		if(is_real_type())
		{
			errval = rsb_mtx_clone(&mtxBp,RSB_NUMERICAL_TYPE_SAME_TYPE,transA, &alpha,this->mtxAp,RSBOI_EXPF);
		}
		else
#if RSBOI_WANT_DOUBLE_COMPLEX
		{
			Complex calpha;calpha+=alpha;
			errval = rsb_mtx_clone(&mtxBp,RSB_NUMERICAL_TYPE_SAME_TYPE,transA,&calpha,this->mtxAp,RSBOI_EXPF);
		}
#else /* RSBOI_WANT_DOUBLE_COMPLEX */
		{RSBOI_0_ERROR(RSBOI_0_NOCOERRMSG);}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
		RSBOI_PERROR(errval);
		return new octave_sparsersb_mtx( mtxBp );
	}

#if RSBOI_WANT_DOUBLE_COMPLEX
	octave_value rsboi_get_scaled_copy(const Complex alpha)const
	{
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		octave_sparsersb_mtx *m = RSBOI_NULL;
		struct rsb_mtx_t *mtxBp = RSBOI_NULL;

		if(is_real_type())
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			errval = rsb_mtx_clone(&mtxBp,RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX,RSB_TRANSPOSITION_N,&rsboi_pone,this->mtxAp,RSBOI_EXPF);
			rsboi_error(errval);
			errval = rsb_mtx_upd_values(mtxBp,RSB_ELOPF_MUL,&alpha);
		}
		else
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			errval = rsb_mtx_clone(&mtxBp,RSB_NUMERICAL_TYPE_SAME_TYPE,RSB_TRANSPOSITION_N,&alpha,this->mtxAp,RSBOI_EXPF);
		}
		rsboi_error(errval);
		m = new octave_sparsersb_mtx( mtxBp );
		if(!m)
			error ("%s", "copying matrix failed!");
		return m;
	}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

octave_value scale_rows(const octave_matrix&v2, bool want_div=false)
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

	if(this->is_real_type())
	{
		const Matrix rm = want_div?1.0/v2.matrix_value ():v2.matrix_value ();
		const octave_idx_type b_nc = rm.cols ();
		const octave_idx_type b_nr = rm.rows ();
		//octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = this->columns();
		const octave_idx_type nrhs = b_nc;
		Matrix retval(ldc,nrhs,RSBOI_ZERO);
		if(this->rows()!=b_nr) { error ("%s", "matrices dimensions do not match!\n"); return Matrix(); }
		errval = rsb_mtx_upd_values(this->mtxAp,RSB_ELOPF_SCALE_ROWS,rm.data());
		RSBOI_PERROR(errval);
		return retval;
	}
	else
	{
		const ComplexMatrix cm = want_div?1.0/v2.complex_matrix_value ():v2.complex_matrix_value ();
		const octave_idx_type b_nc = cm.cols ();
		const octave_idx_type b_nr = cm.rows ();
		//const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = this->columns();
		const octave_idx_type nrhs = b_nc;
		ComplexMatrix retval(ldc,nrhs,RSBOI_ZERO);
		if(this->rows()!=b_nr) { error ("%s", "matrices dimensions do not match!\n"); return ComplexMatrix(); }
		errval = rsb_mtx_upd_values(this->mtxAp,RSB_ELOPF_SCALE_ROWS,cm.data());
		RSBOI_PERROR(errval);
		return retval;
	}
}

octave_value rsboi_spmm(const octave_matrix&v2)const
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	rsb_trans_t transA = RSB_TRANSPOSITION_N;

	if(this->is_real_type())
	{
    const Matrix b = v2.matrix_value ();
		const octave_idx_type b_nc = b.cols ();
		const octave_idx_type b_nr = b.rows ();
		const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = this->rows();
		const octave_idx_type nrhs = b_nc;
		Matrix retval(ldc,nrhs,RSBOI_ZERO);

		if (this->columns()!=b_nr) { error ("%s", "matrix columns count does not match operand rows!\n"); return Matrix(); }
		errval = rsb_spmm(transA,&rsboi_pone,this->mtxAp,nrhs,RSB_OI_DMTXORDER,(RSBOI_T*)b.data(),ldb,&rsboi_pone,(RSBOI_T*)retval.data(),ldc);
		RSBOI_PERROR(errval);
		return retval;
	}
	else
	{
    const ComplexMatrix b = v2.complex_matrix_value ();
		const octave_idx_type b_nc = b.cols ();
		const octave_idx_type b_nr = b.rows ();
		const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = this->rows();
		const octave_idx_type nrhs = b_nc;
		ComplexMatrix retval(ldc,nrhs,RSBOI_ZERO);

		if(this->columns()!=b_nr) { error ("%s", "matrix columns count does not match operand rows!\n"); return Matrix(); }
		errval = rsb_spmm(transA,&rsboi_pone,this->mtxAp,nrhs,RSB_OI_DMTXORDER,(RSBOI_T*)b.data(),ldb,&rsboi_pone,(RSBOI_T*)retval.data(),ldc);
		RSBOI_PERROR(errval);
		return retval;
	}
}

octave_value rsboi_spmtm(const octave_matrix&v2)const
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	rsb_trans_t transA = RSB_TRANSPOSITION_T;

	if(this->is_real_type())
	{
    const Matrix b = v2.matrix_value ();
		const octave_idx_type b_nc = b.cols ();
		const octave_idx_type b_nr = b.rows ();
		const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = this->columns();
		const octave_idx_type nrhs = b_nc;
		Matrix retval(ldc,nrhs,RSBOI_ZERO);

		if(this->rows()   !=b_nr) { error ("%s", "matrix rows count does not match operand rows!\n"); return Matrix(); }
		errval = rsb_spmm(transA,&rsboi_pone,this->mtxAp,nrhs,RSB_OI_DMTXORDER,(RSBOI_T*)b.data(),ldb,&rsboi_pone,(RSBOI_T*)retval.data(),ldc);
		RSBOI_PERROR(errval);
		return retval;
	}
	else
	{
    const ComplexMatrix b = v2.complex_matrix_value ();
		const octave_idx_type b_nc = b.cols ();
		const octave_idx_type b_nr = b.rows ();
		const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = this->columns();
		const octave_idx_type nrhs = b_nc;
		ComplexMatrix retval(ldc,nrhs,RSBOI_ZERO);

		if(this->rows()   !=b_nr) { error ("%s", "matrix rows count does not match operand rows!\n"); return Matrix(); }
		errval = rsb_spmm(transA,&rsboi_pone,this->mtxAp,nrhs,RSB_OI_DMTXORDER,(RSBOI_T*)b.data(),ldb,&rsboi_pone,(RSBOI_T*)retval.data(),ldc);
		RSBOI_PERROR(errval);
		return retval;
	}
}

octave_value rsboi_spmhm(const octave_matrix&v2)const
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	rsb_trans_t transA = RSB_TRANSPOSITION_C;

	if(this->is_real_type())
	{
    const Matrix b = v2.matrix_value ();
		const octave_idx_type b_nc = b.cols ();
		const octave_idx_type b_nr = b.rows ();
		const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = this->columns();
		const octave_idx_type nrhs = b_nc;
		Matrix retval(ldc,nrhs,RSBOI_ZERO);

		if(this->rows()   !=b_nr) { error ("%s", "matrix rows count does not match operand rows!\n"); return Matrix(); }
		errval = rsb_spmm(transA,&rsboi_pone,this->mtxAp,nrhs,RSB_OI_DMTXORDER,(RSBOI_T*)b.data(),ldb,&rsboi_pone,(RSBOI_T*)retval.data(),ldc);
		RSBOI_PERROR(errval);
		return retval;
	}
	else
	{
    const ComplexMatrix b = v2.complex_matrix_value ();
		const octave_idx_type b_nc = b.cols ();
		const octave_idx_type b_nr = b.rows ();
		const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = this->columns();
		const octave_idx_type nrhs = b_nc;
		ComplexMatrix retval(ldc,nrhs,RSBOI_ZERO);

		if(this->rows()   !=b_nr) { error ("%s", "matrix rows count does not match operand rows!\n"); return Matrix(); }
		errval = rsb_spmm(transA,&rsboi_pone,this->mtxAp,nrhs,RSB_OI_DMTXORDER,(RSBOI_T*)b.data(),ldb,&rsboi_pone,(RSBOI_T*)retval.data(),ldc);
		RSBOI_PERROR(errval);
		return retval;
	}
}

#if RSBOI_WANT_DOUBLE_COMPLEX
octave_value rsboi_spmm(const octave_complex_matrix&v2)const
	{

	//	TODO: to avoid e.g. v2.complex_matrix_value, one may use: dim_vector  dv = v2.dims(); ... dv(ndims) ...

	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	const rsb_trans_t transA = RSB_TRANSPOSITION_N;
	struct rsb_mtx_t *mtxCp = RSBOI_NULL;
	const ComplexMatrix b = v2.complex_matrix_value ();
	const octave_idx_type b_nc = b.cols ();
	const octave_idx_type b_nr = b.rows ();
	const octave_idx_type ldb = b_nr;
	const octave_idx_type ldc = this->rows();
	const octave_idx_type nrhs = b_nc;
	ComplexMatrix retval(ldc,nrhs,RSBOI_ZERO); // zeroing is in principle unnecessary (we zero in rsb_spmm), but otherwise data may not be allocated.
	RSBOI_T* Cp =(RSBOI_T*)retval.data();
	const RSBOI_T* Bp =(RSBOI_T*)b.data();

	if(this->is_real_type())
	{
		errval = rsb_mtx_clone(&mtxCp,RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX,RSB_TRANSPOSITION_N,RSBOI_NULL,this->mtxAp,RSBOI_EXPF);
	}
	else{
		mtxCp = this->mtxAp;
	}
	if(RSBOI_SOME_ERROR(errval))
		goto err;

	if(this->columns()!=b_nr) { error ("%s", "matrix columns count does not match operand rows!\n"); return Matrix(); }

	errval = rsb_spmm(transA,&rsboi_pone,mtxCp,nrhs,RSB_OI_DMTXORDER,Bp,ldb,&rsboi_pone,Cp,ldc);

	if(this->is_real_type())
		RSBOI_DESTROY(mtxCp);
err:
	RSBOI_PERROR(errval);
	return retval;
}

octave_value rsboi_spmtm(const octave_complex_matrix&v2)const
	{

	//	TODO: to avoid e.g. v2.complex_matrix_value, one may use: dim_vector  dv = v2.dims(); ... dv(ndims) ...

	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	const rsb_trans_t transA = RSB_TRANSPOSITION_T;
	struct rsb_mtx_t *mtxCp = RSBOI_NULL;
	const ComplexMatrix b = v2.complex_matrix_value ();
	const octave_idx_type b_nc = b.cols ();
	const octave_idx_type b_nr = b.rows ();
	const octave_idx_type ldb = b_nr;
	const octave_idx_type ldc = this->columns();
	const octave_idx_type nrhs = b_nc;
	ComplexMatrix retval(ldc,nrhs,RSBOI_ZERO); // zeroing is in principle unnecessary (we zero in rsb_spmm), but otherwise data may not be allocated.
	RSBOI_T* Cp =(RSBOI_T*)retval.data();
	const RSBOI_T* Bp =(RSBOI_T*)b.data();

	if(this->is_real_type())
		errval = rsb_mtx_clone(&mtxCp,RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX,RSB_TRANSPOSITION_N,RSBOI_NULL,this->mtxAp,RSBOI_EXPF);
	else
		mtxCp = this->mtxAp;
	if(RSBOI_SOME_ERROR(errval))
		goto err;

	if(this->rows()   !=b_nr) { error ("%s", "matrix rows count does not match operand rows!\n"); return Matrix(); }

	errval = rsb_spmm(transA,&rsboi_pone,mtxCp,nrhs,RSB_OI_DMTXORDER,Bp,ldb,&rsboi_pone,Cp,ldc);

	if(this->is_real_type())
		RSBOI_DESTROY(mtxCp);
err:
	RSBOI_PERROR(errval);
	return retval;
}

octave_value rsboi_spmhm(const octave_complex_matrix&v2)const
{

	//	TODO: to avoid e.g. v2.complex_matrix_value, one may use: dim_vector  dv = v2.dims(); ... dv(ndims) ...

	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	const rsb_trans_t transA = RSB_TRANSPOSITION_C;
	struct rsb_mtx_t *mtxCp = RSBOI_NULL;
	const ComplexMatrix b = v2.complex_matrix_value ();
	const octave_idx_type b_nc = b.cols ();
	const octave_idx_type b_nr = b.rows ();
	const octave_idx_type ldb = b_nr;
	const octave_idx_type ldc = this->columns();
	const octave_idx_type nrhs = b_nc;
	ComplexMatrix retval(ldc,nrhs,RSBOI_ZERO); // zeroing is in principle unnecessary (we zero in rsb_spmm), but otherwise data may not be allocated.
	RSBOI_T* Cp =(RSBOI_T*)retval.data();
	const RSBOI_T* Bp =(RSBOI_T*)b.data();

	if(this->is_real_type())
		errval = rsb_mtx_clone(&mtxCp,RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX,RSB_TRANSPOSITION_N,RSBOI_NULL,this->mtxAp,RSBOI_EXPF);
	else
		mtxCp = this->mtxAp;
	if(RSBOI_SOME_ERROR(errval))
		goto err;

	if(this->rows()   !=b_nr) { error ("%s", "matrix rows count does not match operand rows!\n"); return Matrix(); }

	errval = rsb_spmm(transA,&rsboi_pone,mtxCp,nrhs,RSB_OI_DMTXORDER,Bp,ldb,&rsboi_pone,Cp,ldc);

	if(this->is_real_type())
		RSBOI_DESTROY(mtxCp);
err:
	RSBOI_PERROR(errval);
	return retval;
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

octave_value rsboi_spmsp(const octave_sparsersb_mtx&v2)const
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	octave_sparsersb_mtx*sm = new octave_sparsersb_mtx();
	octave_value retval = sm;

#if RSBOI_WANT_SYMMETRY
	/* NOTE: no expansion */
#endif
	/* NOTE: what if they are not both of the same type ? it would be nice to have a conversion.. */
	sm->mtxAp = rsb_spmsp(RSBOI_BINOP_PREVAILING_TYPE(*this,v2),RSB_TRANSPOSITION_N,&rsboi_pone,this->mtxAp,RSB_TRANSPOSITION_N,&rsboi_pone,v2.mtxAp,&errval);
	RSBOI_PERROR(errval);
	if(!sm->mtxAp)
		RSBOI_0_ERROR(RSBOI_0_ALLERRMSG);
	return retval;
}

octave_value rsboi_sppsp(const RSBOI_T*betap, const octave_sparsersb_mtx&v2)const
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	octave_sparsersb_mtx*sm = new octave_sparsersb_mtx();
	octave_value retval = sm;
	rsb_err_t errval = RSB_ERR_NO_ERROR;

	RSBOI_FIXME("");
#if RSBOI_WANT_SYMMETRY
	/* NOTE: no expansion */
#endif

	if ( RSB_LIBRSB_VER < 10201 && ( RSBOI_BINOP_PREVAILING_TYPE(*this,v2) != this->rsbtype() ) )
	{
		/* Way around a bug fixed in librsb-1.2.0.9 */
		struct rsb_mtx_t *mtxCp = RSBOI_NULL;
		errval = rsb_mtx_clone(&mtxCp,RSBOI_BINOP_PREVAILING_TYPE(*this,v2),RSB_TRANSPOSITION_N,RSBOI_NULL,this->mtxAp,RSBOI_EXPF);
		if(RSBOI_SOME_ERROR(errval))
			goto err;
		RSBOI_PERROR(errval);
		sm->mtxAp = rsb_sppsp(RSBOI_BINOP_PREVAILING_TYPE(*this,v2),RSB_TRANSPOSITION_N,&rsboi_pone,mtxCp,RSB_TRANSPOSITION_N,betap,v2.mtxAp,&errval);
		RSBOI_DESTROY(mtxCp);
	}
	else
		sm->mtxAp = rsb_sppsp(RSBOI_BINOP_PREVAILING_TYPE(*this,v2),RSB_TRANSPOSITION_N,&rsboi_pone,this->mtxAp,RSB_TRANSPOSITION_N,betap,v2.mtxAp,&errval);
	RSBOI_PERROR(errval);
err:
	if(!sm->mtxAp)
		RSBOI_0_ERROR(RSBOI_0_ALLERRMSG);
	return retval;
}

#if RSBOI_WANT_DOUBLE_COMPLEX
octave_value cp_ubop(enum rsb_elopf_t opf, Complex z)const
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	octave_sparsersb_mtx *m = new octave_sparsersb_mtx(*this);

	if( is_real_type ())
	{
		struct rsb_mtx_t *mtxCp = RSBOI_NULL;
		errval = rsb_mtx_clone(&mtxCp,RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX,RSB_TRANSPOSITION_N,RSBOI_NULL,this->mtxAp,RSBOI_EXPF);
		if(RSBOI_SOME_ERROR(errval))
			goto err;
		errval = rsb_mtx_upd_values(mtxCp,opf,&z);
		rsboi_error(errval);
		RSBOI_DESTROY(m->mtxAp);
		m->mtxAp = mtxCp;
	}
	else
		errval = rsb_mtx_upd_values(m->mtxAp,opf,&z);
	rsboi_error(errval);
err:
	return m;
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

octave_value cp_ubop(enum rsb_elopf_t opf, void*alphap=RSBOI_NULL)const
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	octave_sparsersb_mtx *m = new octave_sparsersb_mtx(*this);

	if(!m)return m;
	errval = rsb_mtx_upd_values(m->mtxAp,opf,alphap);
	rsboi_error(errval);
	return m;
}

	private:
	public:
			DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA
};/* end of class octave_sparsersb_mtx definition  */

#if 0
octave_value_list find_nonzero_elem_idx (const class octave_sparsersb_mtx & nda, int nargout, octave_idx_type n_to_find, int direction)
{
	// useless
	octave_value retval;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return retval;
}
#endif

#if defined(RSBOI_USE_PATCH_38143)
#define RSBOI_CAST_CONV_ARG(ARGT) /* Seems like in 4.1.0+ CAST_CONV_ARG is not there. */	\
        ARGT v = dynamic_cast< ARGT > (a)
#define RSBOI_CAST_UNOP_ARG(ARGT) /* Seems like in 4.1.0+ CAST_UNOP_ARG is not there. */	\
	RSBOI_CAST_CONV_ARG(ARGT)
#define RSB_CAST_BINOP_ARGS(ARGT_V1, ARGT_V2); /* Seems like in 4.1.0+ CAST_BINOP_ARGS is not there. */	\
        ARGT_V1 v1 = dynamic_cast< ARGT_V1 > (a1);			\
        ARGT_V2 v2 = dynamic_cast< ARGT_V2 > (a2);
#else  /* RSBOI_USE_PATCH_38143 */
#define RSBOI_CAST_CONV_ARG CAST_CONV_ARG
#define RSBOI_CAST_UNOP_ARG CAST_UNOP_ARG
#define RSB_CAST_BINOP_ARGS CAST_BINOP_ARGS
#endif /* RSBOI_USE_PATCH_38143 */

static octave_base_value *default_numeric_conversion_function (const octave_base_value& a)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_CAST_CONV_ARG (const octave_sparsersb_mtx&);
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	RSBOI_WARN(RSBOI_0_UNFFEMSG);
	if(v.is_real_type())
		return new octave_sparse_matrix (v.sparse_matrix_value());
	else
		return new octave_sparse_complex_matrix (v.sparse_complex_matrix_value());
}

DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (octave_sparsersb_mtx,
RSB_OI_TYPEINFO_STRING,
RSB_OI_TYPEINFO_TYPE)

DEFCONV (octave_triangular_conv, octave_sparsersb_mtx, matrix)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	RSBOI_CAST_CONV_ARG (const octave_sparsersb_mtx&);
	return new octave_sparse_matrix (v.matrix_value ());
}

#if 0
DEFCONV (octave_sparse_rsb_to_octave_sparse_conv, sparse_rsb_mtx, sparse_matrix)
{
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_CAST_CONV_ARG (const octave_sparsersb_mtx&);
	return new octave_sparse_matrix (v.matrix_value ());
}
#endif

DEFUNOP (uplus, sparse_rsb_mtx)
{
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_CAST_UNOP_ARG (const octave_sparsersb_mtx&);
	return new octave_sparsersb_mtx (v);
}

#if 0
DEFUNOP (op_incr, sparse_rsb_mtx)
{
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_CAST_UNOP_ARG (const octave_sparsersb_mtx&);
	const octave_idx_type rn = v.mtxAp->nrA,cn = v.mtxAp->ncA;
	Matrix v2(rn,cn);
	octave_value retval = v2;
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	errval|=rsb_mtx_add_to_dense(&rsboi_pone,v.mtxAp,rn,rn,cn,RSB_BOOL_TRUE,(RSBOI_T*)v2.data());
	//v = octave_ma(idx, v2.matrix_value());
	return v2;
}

DEFUNOP (op_decr, sparse_rsb_mtx)
{
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_CAST_UNOP_ARG (const octave_sparsersb_mtx&);
	const octave_idx_type rn = v.mtxAp->nrA, cn = v.mtxAp->ncA;
	Matrix v2(rn,cn);
	octave_value retval = v2;
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	errval|=rsb_mtx_add_to_dense(&rsboi_pone,v.mtxAp,rn,rn,cn,RSB_BOOL_TRUE,(RSBOI_T*)v2.data());
	//v = octave_ma(idx, v2.matrix_value());
	return v2;
}
#endif

DEFUNOP (uminus, sparse_rsb_mtx)
{
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_CAST_UNOP_ARG (const octave_sparsersb_mtx&);
	return v.cp_ubop(RSB_ELOPF_NEG);
}

DEFUNOP (transpose, sparse_rsb_mtx)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_CAST_UNOP_ARG (const octave_sparsersb_mtx&);
	return v.rsboi_get_scaled_copy(rsboi_pone[0],RSB_TRANSPOSITION_T);
}

DEFUNOP (htranspose, sparse_rsb_mtx)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_CAST_UNOP_ARG (const octave_sparsersb_mtx&);
	return v.rsboi_get_scaled_copy(rsboi_pone[0],RSB_TRANSPOSITION_C);
}

octave_value rsboi_spsm(const octave_sparsersb_mtx&v1, const octave_matrix&v2, rsb_trans_t transA)
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

	if(v1.iscomplex())
	{
		ComplexMatrix retval = v2.complex_matrix_value();
		const octave_idx_type b_nc = retval.cols ();
		const octave_idx_type b_nr = retval.rows ();
		const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = v1.rows();
		const octave_idx_type nrhs = b_nc;
		const octave_idx_type nels = retval.rows()*retval.cols();
		errval = rsb_spsm(transA,&rsboi_pone,v1.mtxAp,nrhs,RSB_OI_DMTXORDER,&rsboi_zero,(const RSBOI_T*)retval.data(),ldb,(RSBOI_T*)retval.data(),ldc);
		if(RSBOI_SOME_ERROR(errval))
		{
			RSBOI_PERROR(errval);
			if(errval == RSB_ERR_INVALID_NUMERICAL_DATA)
				error ("%s", RSBOI_0_ZODERRMG);
			for(octave_idx_type i=0;i<nels;++i)
				retval(i)=octave_NaN;
		}
		return retval;
	}
	else
	{
		Matrix retval = v2.matrix_value();
		const octave_idx_type b_nc = retval.cols ();
		const octave_idx_type b_nr = retval.rows ();
		const octave_idx_type ldb = b_nr;
		const octave_idx_type ldc = v1.rows();
		const octave_idx_type nrhs = b_nc;
		const octave_idx_type nels = retval.rows()*retval.cols();

		errval = rsb_spsm(transA,&rsboi_pone,v1.mtxAp,nrhs,RSB_OI_DMTXORDER,&rsboi_zero,(const RSBOI_T*)retval.data(),ldb,(RSBOI_T*)retval.data(),ldc);

		if(RSBOI_SOME_ERROR(errval))
		{
			RSBOI_PERROR(errval);
			if(errval == RSB_ERR_INVALID_NUMERICAL_DATA)
				error ("%s", RSBOI_0_ZODERRMG);
			for(octave_idx_type i=0;i<nels;++i)
				retval(i)=octave_NaN;
		}
		return retval;
	}
}

#if RSBOI_WANT_DOUBLE_COMPLEX
octave_value rsboi_spsm(const octave_sparsersb_mtx&v1, const octave_complex_matrix&v2, rsb_trans_t transA)
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	ComplexMatrix retval = v2.complex_matrix_value();
	const octave_idx_type b_nc = retval.cols ();
	const octave_idx_type b_nr = retval.rows ();
	const octave_idx_type ldb = b_nr;
	const octave_idx_type ldc = v1.rows();
	const octave_idx_type nrhs = b_nc;
	const octave_idx_type nels = retval.rows()*retval.cols();
	struct rsb_mtx_t *mtxCp = RSBOI_NULL;

	if(v1.is_real_type())
		errval = rsb_mtx_clone(&mtxCp,RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX,RSB_TRANSPOSITION_N,RSBOI_NULL,v1.mtxAp,RSBOI_EXPF);
	else
		mtxCp = v1.mtxAp;
	if(RSBOI_SOME_ERROR(errval))
		goto err;

	errval = rsb_spsm(transA,&rsboi_pone,mtxCp,nrhs,RSB_OI_DMTXORDER,&rsboi_zero,(const RSBOI_T*)retval.data(),ldb,(RSBOI_T*)retval.data(),ldc);

	if(RSBOI_SOME_ERROR(errval))
	{
		RSBOI_PERROR(errval);
		if(errval == RSB_ERR_INVALID_NUMERICAL_DATA)
			error ("%s", RSBOI_0_ZODERRMG);
		for(octave_idx_type i=0;i<nels;++i)
			retval(i)=octave_NaN;
	}
	if(v1.is_real_type())
		RSBOI_DESTROY(mtxCp);
err:
	RSBOI_PERROR(errval);
	return retval;
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

DEFBINOP(ldiv, sparse_rsb_mtx, matrix)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_matrix&);

	if(v1.is__triangular())
		return rsboi_spsm(v1,v2,RSB_TRANSPOSITION_N);

	if(v1.iscomplex() || v2.iscomplex())
		return (v1.sparse_complex_matrix_value()).solve(v2.sparse_complex_matrix_value());
	else
		return (v1.sparse_matrix_value()).solve(v2.matrix_value());
	//RSBOI_RSB_MATRIX_SOLVE(v1,v2);
}

DEFBINOP(trans_ldiv, sparse_rsb_mtx, matrix)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_matrix&);

	if(v1.is__triangular())
		return rsboi_spsm(v1,v2,RSB_TRANSPOSITION_T);

	if(v1.iscomplex() || v2.iscomplex())
		return (v1.sparse_complex_matrix_value().transpose()).solve(v2.sparse_complex_matrix_value());
	else
		return (v1.sparse_matrix_value().transpose()).solve(v2.matrix_value());
	//RSBOI_RSB_MATRIX_SOLVE(v1,v2);
}

#if RSBOI_WANT_DOUBLE_COMPLEX
DEFBINOP(c_ldiv, sparse_rsb_mtx, matrix)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_complex_matrix&);

	if(v1.is__triangular())
		return rsboi_spsm(v1,v2,RSB_TRANSPOSITION_N);

	if(v1.iscomplex() || v2.iscomplex())
		return (v1.sparse_complex_matrix_value()).solve(v2.sparse_complex_matrix_value());
	else
		return (v1.sparse_matrix_value()).solve(v2.matrix_value());
	//RSBOI_RSB_MATRIX_SOLVE(v1,v2);
}

DEFBINOP(trans_c_ldiv, sparse_rsb_mtx, matrix)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_complex_matrix&);
	if(v1.is__triangular())
		return rsboi_spsm(v1,v2,RSB_TRANSPOSITION_T);

	if(v1.iscomplex() || v2.iscomplex())
		return (v1.sparse_complex_matrix_value().transpose()).solve(v2.sparse_complex_matrix_value());
	else
		return (v1.sparse_matrix_value().transpose()).solve(v2.matrix_value());
	//RSBOI_RSB_MATRIX_SOLVE(v1,v2);
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

DEFBINOP(el_div, sparse_rsb_mtx, matrix)
{
	Matrix retval;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	return retval;
}

DEFBINOP(el_ldiv, sparse_rsb_mtx, matrix)
{
	Matrix retval;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	return retval;
}

DEFBINOP(div, sparse_rsb_mtx, matrix)
{
	Matrix retval;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	return retval;
}

#if RSBOI_WANT_DOUBLE_COMPLEX
DEFBINOP(rsb_c_div, sparse_rsb_mtx, complex)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_complex&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy_inv(v2.complex_value());
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

DEFBINOP(rsb_s_div, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_scalar&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy_inv(v2.scalar_value());
}

DEFBINOP(rsb_s_mul, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_scalar&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy(v2.scalar_value());
}

DEFBINOP(s_rsb_mul, scalar, sparse_rsb_mtx)
{
	RSB_CAST_BINOP_ARGS (const octave_scalar&, const octave_sparsersb_mtx &);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v2.rsboi_get_scaled_copy(v1.scalar_value());
}

#if RSBOI_WANT_DOUBLE_COMPLEX
DEFBINOP(rsb_c_mul, sparse_rsb_mtx, complex)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_complex&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy(v2.complex_value());
}

DEFBINOP(c_rsb_mul, complex, sparse_rsb_mtx)
{
	RSB_CAST_BINOP_ARGS (const octave_complex&, const octave_sparsersb_mtx &);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v2.rsboi_get_scaled_copy(v1.complex_value());
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

#if RSBOI_WANT_POW
DEFBINOP(rsb_s_pow, sparse_rsb_mtx, scalar) // ^
{
	RSBOI_FIXME("This is elemental exponentiation!");
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_scalar&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_T alpha = v2.scalar_value();
	return v1.cp_ubop(RSB_ELOPF_POW,&alpha);
}
#endif /* RSBOI_WANT_POW */

DEFASSIGNOP (assign, sparse_rsb_mtx, sparse_rsb_mtx)
{
	rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSBOI_FIXME("I dunno how to trigger this!");
	RSB_CAST_BINOP_ARGS (octave_sparsersb_mtx &, const octave_sparsersb_mtx&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	//rsb_assign(v1.mtxAp, v2.mtxAp);
	errval = rsb_mtx_clone(&v1.mtxAp,RSB_NUMERICAL_TYPE_SAME_TYPE,RSB_TRANSPOSITION_N,RSBOI_NULL,v2.mtxAp,RSBOI_EXPF);
	RSBOI_PERROR(errval);
	return octave_value();
}

DEFASSIGNOP (assignm, sparse_rsb_mtx, matrix)
{
	RSB_CAST_BINOP_ARGS (octave_sparsersb_mtx &, const octave_matrix&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_DESTROY(v1.mtxAp);
	octave_value retval;
	//v1.assign(idx, v2.matrix_value());
#if RSBOI_USE_PATCH_OCT44
	v1.assign(idx, v2.sparse_matrix_value());
#else /* RSBOI_USE_PATCH_OCT44 */
	// would break on octave6 (assignment deleted)
	v1 = (idx, v2.matrix_value());
#endif /* RSBOI_USE_PATCH_OCT44 */
	//retval = v1;
	retval = v2.matrix_value();
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	return retval;
}

#if 0
DEFASSIGNOP(rsb_op_mul_eq_s, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (octave_sparsersb_mtx &, const octave_scalar&);
	octave_value retval;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_PERROR(v1.rsboi_scale(v2.scalar_value()));
	retval = v1.matrix_value();
	return retval;
}

	rsb_err_t rsboi_scale(RSBOI_T alpha)
	{
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		//errval = rsb_elemental_scale(this->mtxAp,&alpha);
	       	errval = rsb_elemental_op(this->mtxAp,RSB_ELOPF_MUL,&alpha);
		RSBOI_PERROR(errval);
		return errval;
	}

	rsb_err_t rsboi_scale(Complex alpha)
	{
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		//errval = rsb_elemental_scale(this->mtxAp,&alpha);
	       	errval = rsb_elemental_op(this->mtxAp,RSB_ELOPF_MUL,&alpha);
		RSBOI_PERROR(errval);
		return errval;
	}

DEFASSIGNOP(rsb_op_div_eq_s, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (octave_sparsersb_mtx &, const octave_scalar&);
	octave_value retval;
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_PERROR(v1.rsboi_scale_inv(v2.scalar_value()));
	retval = v1.matrix_value();
	return retval;
}

	rsb_err_t rsboi_scale_inv(RSBOI_T alpha)
	{
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		//errval = rsb_elemental_scale_inv(this->mtxAp,&alpha);
	       	errval = rsb_elemental_op(this->mtxAp,RSB_ELOPF_DIV,&alpha);
		RSBOI_PERROR(errval);
		return errval;
	}

	rsb_err_t rsboi_scale_inv(Complex alpha)
	{
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		//errval = rsb_elemental_scale_inv(this->mtxAp,&alpha);
	       	errval = rsb_elemental_op(this->mtxAp,RSB_ELOPF_DIV,&alpha);
		RSBOI_PERROR(errval);
		return errval;
	}
#endif

DEFBINOP(rsb_el_mul_s, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_scalar&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy(v2.scalar_value());
}

#if RSBOI_WANT_DOUBLE_COMPLEX
DEFBINOP(rsb_el_mul_c, sparse_rsb_mtx, complex)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_complex&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy(v2.complex_value());
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

DEFBINOP(rsb_el_div_s, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_scalar&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy_inv(v2.scalar_value());
}

#if RSBOI_WANT_DOUBLE_COMPLEX
DEFBINOP(rsb_el_div_c, sparse_rsb_mtx, complex)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_complex&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy_inv(v2.complex_value());
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

#if RSBOI_WANT_DOUBLE_COMPLEX
#if 0
DEFASSIGNOP(rsb_op_el_div_eq, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_scalar&);
	std::cout << "rsb_op_el_div_eq!\n";
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	return v1.rsboi_get_scaled_copy_inv(v2.complex_value());
}
#endif

DEFASSIGNOP(rsb_op_el_mul_eq_sc, sparse_rsb_mtx, matrix)
{
	//rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSB_CAST_BINOP_ARGS (octave_sparsersb_mtx &, const octave_matrix&);
	return v1.scale_rows(v2,false);
}

DEFASSIGNOP(rsb_op_el_div_eq_sc, sparse_rsb_mtx, matrix)
{
	//rsb_err_t errval = RSB_ERR_NO_ERROR;
	RSB_CAST_BINOP_ARGS (octave_sparsersb_mtx &, const octave_matrix&);
	return v1.scale_rows(v2,true);
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

DEFBINOP(el_pow, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_scalar&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSBOI_T alpha [] = {v2.scalar_value(),0};
	return v1.cp_ubop(RSB_ELOPF_POW,&alpha);
}

#if RSBOI_WANT_DOUBLE_COMPLEX
DEFBINOP(el_pow_c, sparse_rsb_mtx, complex)
{
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx &, const octave_complex&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	Complex alpha = v2.complex_value();
	return v1.cp_ubop(RSB_ELOPF_POW,alpha);
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

#ifdef RSB_FULLY_IMPLEMENTED
DEFASSIGNOP (assigns, sparse_rsb_mtx, scalar)
{
	RSB_CAST_BINOP_ARGS (octave_sparsersb_mtx &, const octave_scalar&);
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	v1.assign(idx, v2.matrix_value());
	RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	return octave_value();
}
#endif

DEFBINOP(op_sub, sparse_rsb_mtx, sparse_rsb_mtx)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_sparsersb_mtx&);
	return v1.rsboi_sppsp(&rsboi_mone[0],v2);
}

DEFBINOP(op_add, sparse_rsb_mtx, sparse_rsb_mtx)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_sparsersb_mtx&);
	return v1.rsboi_sppsp(&rsboi_pone[0],v2);
}

DEFBINOP(op_spmul, sparse_rsb_mtx, sparse_rsb_mtx)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_sparsersb_mtx&);
	return v1.rsboi_spmsp(v2);
}

DEFBINOP(op_mul, sparse_rsb_mtx, matrix)
{
	// "*"  operator
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_matrix&);
	//return v1.rsboi_spmm(v2, false);
	return v1.rsboi_spmm(v2);
}

DEFBINOP(op_trans_mul, sparse_rsb_mtx, matrix)
{
	// ".'*"  operator
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_matrix&);
	//return v1.rsboi_spmm(v2, true);
	return v1.rsboi_spmtm(v2);
}

DEFBINOP(op_herm_mul, sparse_rsb_mtx, matrix)
{
	// "'*"  operator
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_matrix&);
	//return v1.rsboi_spmm(v2, true);
	return v1.rsboi_spmhm(v2);
}

#if RSBOI_WANT_DOUBLE_COMPLEX
DEFBINOP(op_c_mul, sparse_rsb_mtx, matrix)
{
	// "*"  operator
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_complex_matrix&);
	//return v1.rsboi_spmm(v2, false);
	return v1.rsboi_spmm(v2);
}

DEFBINOP(op_c_trans_mul, sparse_rsb_mtx, matrix)
{
	// ".'*"  operator
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_complex_matrix&);
	//return v1.rsboi_spmm(v2, true);
	return v1.rsboi_spmtm(v2);
}

DEFBINOP(op_c_herm_mul, sparse_rsb_mtx, matrix)
{
	// "'*"  operator
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	RSB_CAST_BINOP_ARGS (const octave_sparsersb_mtx&, const octave_complex_matrix&);
	//return v1.rsboi_spmm(v2, true);
	return v1.rsboi_spmhm(v2);
}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

#if RSBOI_USE_PATCH_OCT44
#define RSBOI_INSTALL_BINOP(op, t1, t2, f) { \
  	octave::type_info& type_info = octave::__get_type_info__ ("");\
	type_info.register_binary_op(octave_value::op, t1::static_type_id (), t2::static_type_id (), CONCAT2 (oct_binop_, f)); }

#define RSBOI_INSTALL_ASSIGNOP(op, t1, t2, f) { \
  	octave::type_info& type_info = octave::__get_type_info__ ("");\
	type_info.register_assign_op(octave_value::op, t1::static_type_id (), t2::static_type_id (), CONCAT2 (oct_assignop_, f)); }

#define RSBOI_INSTALL_UNOP(op, t1, f) { \
  	octave::type_info& type_info = octave::__get_type_info__ ("");\
	type_info.register_unary_op(octave_value::op, t1::static_type_id (), CONCAT2 (oct_unop_, f)); }
#else /* RSBOI_USE_PATCH_OCT44 */
// deprecated; need a wrapper using octave::typeinfo::register_binary_op
#define RSBOI_INSTALL_BINOP INSTALL_BINOP

// deprecated; need a wrapper using octave::typeinfo::register_assign_op
#define RSBOI_INSTALL_ASSIGNOP INSTALL_ASSIGNOP

// deprecated; need a wrapper using octave::typeinfo::register_unary_op
#define RSBOI_INSTALL_UNOP INSTALL_UNOP
#endif /* RSBOI_USE_PATCH_OCT44 */

static void install_sparsersb_ops (void)
{
	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
	#ifdef RSB_FULLY_IMPLEMENTED
	/* boolean pattern-based not */
	RSBOI_INSTALL_UNOP (op_not, octave_sparsersb_mtx, op_not);
	/* to-dense operations */
	RSBOI_INSTALL_ASSIGNOP (op_asn_eq, octave_sparsersb_mtx, octave_scalar, assigns);
	/* ? */
	RSBOI_INSTALL_UNOP (op_uplus, octave_sparsersb_mtx, uplus);
	/* elemental comparison, evaluate to sparse or dense boolean matrices */
	RSBOI_INSTALL_BINOP (op_eq, octave_sparsersb_mtx, , );
	RSBOI_INSTALL_BINOP (op_le, octave_sparsersb_mtx, , );
	RSBOI_INSTALL_BINOP (op_lt, octave_sparsersb_mtx, , );
	RSBOI_INSTALL_BINOP (op_ge, octave_sparsersb_mtx, , );
	RSBOI_INSTALL_BINOP (op_gt, octave_sparsersb_mtx, , );
	RSBOI_INSTALL_BINOP (op_ne, octave_sparsersb_mtx, , );
	/* pure elemental; scalar and sparse arguments ?! */
								 // ?
	RSBOI_INSTALL_BINOP (op_el_ldiv, octave_sparsersb_mtx, , );
	RSBOI_INSTALL_BINOP (op_el_ldiv_eq, octave_sparsersb_mtx, , ); // errval = rsb_mtx_upd_values(this->mtxAp,RSB_ELOPF_SCALE_ROWS,cm.data());
	RSBOI_INSTALL_BINOP (op_el_mul_eq, octave_sparsersb_mtx, , ); // diagonal subst ??
	RSBOI_INSTALL_BINOP (op_el_and, octave_sparsersb_mtx, , );
	RSBOI_INSTALL_BINOP (op_el_or, octave_sparsersb_mtx, , );
	/* shift operations: they may be left out from the implementation */
	RSBOI_INSTALL_BINOP (op_lshift, octave_sparsersb_mtx, , );
	RSBOI_INSTALL_BINOP (op_rshift, octave_sparsersb_mtx, , );
	#endif
	// RSBOI_INSTALL_ASSIGNOP (op_el_div_eq, octave_sparsersb_mtx, octave_matrix, rsb_op_el_div_eq_sc); // errval = rsb_mtx_upd_values(this->mtxAp,RSB_ELOPF_SCALE_ROWS,cm.data());
	// RSBOI_INSTALL_ASSIGNOP (op_el_mul_eq, octave_sparsersb_mtx, octave_matrix, rsb_op_el_mul_eq_sc);
	//INSTALL_WIDENOP (octave_sparsersb_mtx, octave_sparse_matrix,octave_sparse_rsb_to_octave_sparse_conv);/* a DEFCONV .. */
	//INSTALL_ASSIGNCONV (octave_sparsersb_mtx, octave_sparse_matrix,octave_sparse_matrix);/* .. */
	// no need for the following: need a good conversion function, though
	//RSBOI_INSTALL_UNOP (op_incr, octave_sparsersb_mtx, op_incr);
	//RSBOI_INSTALL_UNOP (op_decr, octave_sparsersb_mtx, op_decr);
	RSBOI_INSTALL_BINOP (op_el_mul, octave_sparsersb_mtx, octave_scalar, rsb_el_mul_s);
#if RSBOI_WANT_DOUBLE_COMPLEX
	RSBOI_INSTALL_BINOP (op_el_mul, octave_sparsersb_mtx, octave_complex, rsb_el_mul_c);
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
//	RSBOI_INSTALL_ASSIGNOP (op_mul_eq, octave_sparsersb_mtx, octave_scalar, rsb_op_mul_eq_s); // 20110313 not effective
//	RSBOI_INSTALL_ASSIGNOP (op_div_eq, octave_sparsersb_mtx, octave_scalar, rsb_op_div_eq_s); // 20110313 not effective
	RSBOI_INSTALL_BINOP (op_el_div, octave_sparsersb_mtx, octave_scalar, rsb_el_div_s);
#if RSBOI_WANT_DOUBLE_COMPLEX
	RSBOI_INSTALL_BINOP (op_el_div, octave_sparsersb_mtx, octave_complex, rsb_el_div_c);
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
	RSBOI_INSTALL_BINOP (op_el_pow, octave_sparsersb_mtx, octave_scalar, el_pow);
	RSBOI_INSTALL_BINOP (op_el_pow, octave_sparsersb_mtx, octave_complex, el_pow_c);
	RSBOI_INSTALL_UNOP (op_uminus, octave_sparsersb_mtx, uminus);
	RSBOI_INSTALL_BINOP (op_ldiv, octave_sparsersb_mtx, octave_matrix, ldiv);
	RSBOI_INSTALL_BINOP (op_el_ldiv, octave_sparsersb_mtx, octave_matrix, el_ldiv);
	RSBOI_INSTALL_BINOP (op_div, octave_sparsersb_mtx, octave_matrix, div);
	RSBOI_INSTALL_BINOP (op_div, octave_sparsersb_mtx, octave_scalar, rsb_s_div);
#if RSBOI_WANT_DOUBLE_COMPLEX
	RSBOI_INSTALL_BINOP (op_div, octave_sparsersb_mtx, octave_complex, rsb_c_div);
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
	RSBOI_INSTALL_BINOP (op_mul, octave_sparsersb_mtx, octave_scalar, rsb_s_mul);
	RSBOI_INSTALL_BINOP (op_mul, octave_scalar, octave_sparsersb_mtx, s_rsb_mul);
#if RSBOI_WANT_DOUBLE_COMPLEX
	RSBOI_INSTALL_BINOP (op_mul, octave_sparsersb_mtx, octave_complex, rsb_c_mul);
	RSBOI_INSTALL_BINOP (op_mul, octave_complex, octave_sparsersb_mtx, c_rsb_mul);
	RSBOI_INSTALL_BINOP (op_mul, octave_sparsersb_mtx, octave_complex_matrix, op_c_mul);
	RSBOI_INSTALL_BINOP (op_trans_mul, octave_sparsersb_mtx, octave_complex_matrix, op_c_trans_mul);
  RSBOI_INSTALL_BINOP (op_herm_mul, octave_sparsersb_mtx, octave_complex_matrix, op_c_herm_mul);
	RSBOI_INSTALL_BINOP (op_ldiv, octave_sparsersb_mtx, octave_complex_matrix, c_ldiv);
	RSBOI_INSTALL_BINOP (op_trans_ldiv, octave_sparsersb_mtx, octave_complex_matrix, trans_c_ldiv);
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
#if RSBOI_WANT_POW
	RSBOI_INSTALL_BINOP (op_pow, octave_sparsersb_mtx, octave_scalar, rsb_s_pow);
#endif /* RSBOI_WANT_POW */
	RSBOI_INSTALL_BINOP (op_el_div, octave_sparsersb_mtx, octave_matrix, el_div);
	RSBOI_INSTALL_UNOP (op_transpose, octave_sparsersb_mtx, transpose);
	RSBOI_INSTALL_UNOP (op_hermitian, octave_sparsersb_mtx, htranspose);
	RSBOI_INSTALL_ASSIGNOP (op_asn_eq, octave_sparsersb_mtx, octave_sparse_matrix, assign);
	RSBOI_INSTALL_ASSIGNOP (op_asn_eq, octave_sparsersb_mtx, octave_matrix, assignm);
	RSBOI_INSTALL_BINOP (op_mul, octave_sparsersb_mtx, octave_matrix, op_mul);
	//RSBOI_INSTALL_BINOP (op_pow, octave_sparsersb_mtx, octave_matrix, op_pow);
	RSBOI_INSTALL_BINOP (op_sub, octave_sparsersb_mtx, octave_sparsersb_mtx, op_sub);
	RSBOI_INSTALL_BINOP (op_add, octave_sparsersb_mtx, octave_sparsersb_mtx, op_add);
	//RSBOI_INSTALL_BINOP (op_trans_add, octave_sparsersb_mtx, octave_sparsersb_mtx, op_trans_add);
	RSBOI_INSTALL_BINOP (op_mul, octave_sparsersb_mtx, octave_sparsersb_mtx, op_spmul);
	RSBOI_INSTALL_BINOP (op_trans_mul, octave_sparsersb_mtx, octave_matrix, op_trans_mul);
	RSBOI_INSTALL_BINOP (op_herm_mul, octave_sparsersb_mtx, octave_matrix, op_herm_mul);
	RSBOI_INSTALL_BINOP (op_trans_ldiv, octave_sparsersb_mtx, octave_matrix, trans_ldiv);
	//RSBOI_INSTALL_BINOP (op_mul_trans, octave_sparsersb_mtx, octave_matrix, op_mul_trans);
	//RSBOI_INSTALL_BINOP (op_mul_trans, octave_sparsersb_mtx, octave_matrix, op_mul_trans);
	//RSBOI_INSTALL_BINOP (op_herm_mul, octave_sparsersb_mtx, octave_matrix, op_herm_mul);
	//RSBOI_INSTALL_BINOP (op_mul_herm, octave_sparsersb_mtx, octave_matrix, op_mul_herm);
	//RSBOI_INSTALL_BINOP (op_el_not_and, octave_sparsersb_mtx, octave_matrix, op_el_not_and);
	//RSBOI_INSTALL_BINOP (op_el_not_or , octave_sparsersb_mtx, octave_matrix, op_el_not_or );
	//RSBOI_INSTALL_BINOP (op_el_and_not, octave_sparsersb_mtx, octave_matrix, op_el_and_not);
	//RSBOI_INSTALL_BINOP (op_el_or _not, octave_sparsersb_mtx, octave_matrix, op_el_or _not);
}

static void install_sparse_rsb (void)
{
	static bool rsboi_initialized = false;

	RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);

	if(!rsboi_initialized)
	{
		rsb_err_t errval = RSB_ERR_NO_ERROR;

		if(sparsersb_tester() == false)
		{
			goto err;
		}
		errval = rsb_lib_init(RSB_NULL_INIT_OPTIONS);
		if(RSBOI_SOME_ERROR(errval))
		{
			RSBOI_PERROR(errval);
			goto err;
		}
		rsboi_initialized = true;
	}
	else
		;/* already initialized */

	if (!rsboi_sparse_rsb_loaded)
	{
		octave_sparsersb_mtx::register_type ();
		install_sparsersb_ops ();
		rsboi_sparse_rsb_loaded = true;

#if RSBOI_USE_PATCH_OCT44
		octave::interpreter::the_interpreter()->mlock();
#else /* RSBOI_USE_PATCH_OCT44 */
		mlock();
#endif /* RSBOI_USE_PATCH_OCT44 */
	}
	return;
err:
	RSBIO_NULL_STATEMENT_FOR_COMPILER_HAPPINESS
} /* install_sparse_rsb */

DEFUN_DLD (RSB_SPARSERSB_LABEL, args, nargout,
"-*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{S} =} " RSBOI_FNS " (@var{A})\n\
@deftypefnx {Loadable Function} {@var{S} =} " RSBOI_FNS " (@var{I}, @var{J}, @var{SV}, @var{M}, @var{N})\n\
@deftypefnx {Loadable Function} {@var{S} =} " RSBOI_FNS " (@var{I}, @var{J}, @var{SV}, @var{M}, @var{N}, @var{NZMAX})\n\
@deftypefnx {Loadable Function} {@var{S} =} " RSBOI_FNS " (@var{I}, @var{J}, @var{SV})\n\
@deftypefnx {Loadable Function} {@var{S} =} " RSBOI_FNS " (@var{M}, @var{N})\n\
@deftypefnx {Loadable Function} {@var{S} =} " RSBOI_FNS " (@var{I}, @var{J}, @var{SV}, @var{M}, @var{N}, \"unique\")\n\
@deftypefnx {Loadable Function}             " RSBOI_FNS " (\"set\", @var{OPN}, @var{OPV})\n\
@deftypefnx {Loadable Function} {@var{v} =} " RSBOI_FNS " (@var{S}, \"get\", @var{MIF})\n\
@deftypefnx {Loadable Function} {@var{v} =} " RSBOI_FNS " (@var{S}, @var{QS})\n\
@deftypefnx {Loadable Function} " RSBOI_FNS " (@var{A},\"save\",@var{MTXFILENAME})\n\
@deftypefnx {Loadable Function} {[@var{S}[, @var{nrows}[, @var{NCOLS}[, @var{NNZ}[, @var{REPINFO}[, @var{FIELD}[, @var{SYMMETRY}]]]]]]] =} " RSBOI_FNS " (@var{MTXFILENAME}[, @var{MTXTYPESTRING}])\n\
" RSBOI_10100_DOCH ""\
\
"\n"\
"Create or manipulate sparse matrices using the RSB format provided by librsb, almost as you do with @code{sparse}.\n"\
"\n"\
"If @var{A} is a @code{full} matrix, convert it to a sparse matrix representation,\n\
removing all zero values.\n"\
"If @var{A} is a @code{sparse} matrix, convert it to a sparse matrix representation.\n"\
"\n\
Given the integer index vectors @var{I} and @var{J}, and a 1-by-@code{nnz}\n\
vector of real or complex values @var{SV}, construct the sparse matrix\n\
@code{S(@var{I}(@var{K}),@var{J}(@var{K})) = @var{SV}(@var{K})} with overall\n\
dimensions @var{M} and @var{N}.  \n\
\nThe argument\n\
@code{@var{NZMAX}} is ignored but accepted for compatibility with @sc{Matlab} and @code{sparse}.\n\
\n\
If @var{M} or @var{N} are not specified their values are derived from the\n\
maximum index in the vectors @var{I} and @var{J} as given by\n\
@code{@var{M} = max (@var{I})}, @code{@var{N} = max (@var{J})}.\n\
\n\
\
Can load a matrix from a Matrix Market matrix file named @var{MTXFILENAME}. \
The optional argument @var{MTXTYPESTRING} can specify either real (@code{\"D\"}) or complex (@code{\"Z\"}) type. \
Default is real.\n"\
"In the case @var{MTXFILENAME} is @code{\"" RSBOI_LIS "\"}, a string listing the available numerical types with BLAS-style characters will be returned. If the file turns out to contain a Matrix Market dense vector, this will be loaded.\n"\
\
\
"\n\
\
If @code{\"save\"} is specified, saves the sparse matrix as a Matrix Market matrix file named @var{MTXFILENAME}.\n"\
"\n\
\
@strong{Note}: if multiple values are specified with the same\n\
@var{I}, @var{J} indices, the corresponding values in @var{SV} will\n\
be added.\n\
\n\
The following are all equivalent:\n\
\n\
@example\n\
@group\n\
S = " RSBOI_FNS " (I, J, SV, M, N)\n\
S = " RSBOI_FNS " (I, J, SV, M, N, \"summation\")\n\
S = " RSBOI_FNS " (I, J, SV, M, N, \"sum\")\n"\
/*"S = " RSBOI_FNS " (I, J, SV, \"summation\")\n"*/\
/*"S = " RSBOI_FNS " (I, J, SV, \"sum\")\n"*/\
"@end group\n\
@end example\n\
\n\
\
If the optional @code{\"unique\"} keyword is specified instead, then if more than two values are specified for the\n\
same @var{I}, @var{J} indices, only the last value will be used.\n\
\n\
\
If the optional @code{\"symmetric\"} or @code{\"sym\"} keyword follows, then the input will be considered as the tringle of a symmetric matrix.\n\
If the optional @code{\"hermitian\"} or @code{\"her\"} keyword follows, then the input will be considered as the tringle of a hermitian matrix.\n\
If the optional @code{\"general\"} or @code{\"gen\"} keyword follows, then no symmetry hint is being given.\n\
\n\
@code{" RSBOI_FNS " (@var{M}, @var{N})} will create an empty @var{M}-by-@var{N} sparse\n\
matrix and is equivalent to @code{" RSBOI_FNS " ([], [], [], @var{M}, @var{N})}.\n\
\n\
\
\n\
\
If @var{M} or @var{N} are not specified, then @code{@var{M} = max (@var{I})}, @code{@var{N} = max (@var{J})}.\n\
\n\
\
If @var{OPN} is a string representing a valid librsb option name and @var{OPV} is a string representing a valid librsb option value, these will be passed to the @code{rsb_lib_set_opt_str()} function.\n\
\n\
\
If @var{MIF} is a string specifying a valid librsb matrix info string (valid for librsb's @code{rsb_mtx_get_info_from_string()}), then the corresponding value will be returned for matrix @code{@var{S}}, in string @code{@var{V}}. If @var{MIF} is the an empty string (@code{\"\"}), matrix structure information will be returned. As of librsb-1.2, this is debug or internal information. E.g. for @code{\"RSB_MIF_LEAVES_COUNT__TO__RSB_BLK_INDEX_T\"}, a string with the count of internal RSB blocks will be returned.\n\
\n"\
\
/*"If @var{S} is a " RSBOI_FNS " matrix and @var{QS} is a string, @var{QS} will be interpreted as a query string about matrix @var{S}. String @code{@var{V}} will be returned. See librsb's @code{rsb_mtx_get_info_str()}.\n\
@strong{Note}: this feature is still incomplete, and whatever the value of @var{QS}, a general information string will be returned.\n"*/\
\
"If @var{S} is a @code{" RSBOI_FNS "} matrix and @var{QS} is a string, @var{QS} shall be interpreted as a query string about matrix @var{S}. String @code{@var{V}} will be returned with query results. \n @strong{Note}: this feature is to be completed and its syntax reserved for future use. In this version, whatever the value of @var{QS}, a general matrix information string will be returned (like @code{" RSBOI_FNS "(@var{S},\"get\",\"RSB_MIF_LEAVES_COUNT__TO__RSB_BLK_INDEX_T\")} ).\n"\
"\n"\
/*If any of @var{SV}, @var{I} or @var{J} are scalars, they are expanded\n\
to have a common size.\n*/
RSBOI_10100_DOC ""\
"\n\
Long (64 bit) index support is partial: if Octave has been configured for 64 bit indices, @code{" RSBOI_FNS "} will correctly handle and convert matrices/indices that would fit in a 32 bit indices setup, failing on 'larger' ones. \n\
\n\
@strong{Note}: @code{" RSBOI_FNS "} variables behave just as @code{full} or @code{sparse} variables for @strong{most} operators.\n\
But interaction of binary sparse matrix -- sparse matrix operators involving @strong{symmetric} @code{" RSBOI_FNS "} matrices is not complete and may give unexpected results.\n\
\n\
@strong{Note}: \
Multiplication of a @code{" RSBOI_FNS "} variable by a @code{sparse} one (or the other way round) will expand @code{" RSBOI_FNS "}'s symmetry because of conversion to @code{sparse}.\n\
Multiplication of two @code{" RSBOI_FNS "} variables will not undergo any conversion or symmetry expansion (which might come as unexpected).\n\
\n\
@strong{Note}: \
Summation of a @code{" RSBOI_FNS "} variable with a @code{sparse} one (or the other way round) will expand @code{" RSBOI_FNS "}'s symmetry because of conversion to @code{sparse}.\n\
Summation of two @code{" RSBOI_FNS "} variables will not undergo any conversion or symmetry expansion (which might come as unexpected).\n\
\n\
@strong{Note}: \
Accessing a symmetric or hermitian @code{" RSBOI_FNS "} variable at indices falling in the empty triangle will return a zero.\n\
Accessing via (:,:) will imply symmetry/hermitianness expansion and conversion to @code{sparse}.\n\
\n\
@seealso{sparse, full, nnz, rows, columns, tril, triu, istril, istriu, issparse, iscomplex, isreal, issymmetric, ishermitian}\n\
@end deftypefn")
{
	int nargin = args.length ();
	octave_value_list retval;
	octave_sparsersb_mtx*osmp = RSBOI_NULL;
	bool ic0 = nargin>0?(args(0).iscomplex()):false;
	bool ic3 = nargin>2?(args(2).iscomplex()):false;
	bool isr = (nargin>0 && args(0).type_name()==RSB_OI_TYPEINFO_STRING);

	RSBOI_DEBUG_NOTICE("in sparsersb()\n");

	if(ic0)
	{
		RSBOI_WARN(RSBOI_O_MISSIMPERRMSG);
	}

	if(isr)
		osmp = ((octave_sparsersb_mtx*)(args(0).internal_rep()));

	if(ic3 || ic0)
#if RSBOI_WANT_DOUBLE_COMPLEX
		RSBOI_WARN(RSBOI_0_UNCFEMSG);
#else /* RSBOI_WANT_DOUBLE_COMPLEX */
		RSBOI_0_ERROR(RSBOI_0_NOCOERRMSG);
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
	install_sparse_rsb();
	if( nargin == 3 && args(0).is_string() && args(0).string_value()=="set" && args(1).is_string() && args(2).is_string())
	{
		// sparsersb ("set", OPN, OPV)
		const auto os = args(1).string_value();
		const auto vs = args(2).string_value();
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		const rsb_err_t errval = rsb_lib_set_opt_str(os.c_str(),vs.c_str());
		if(RSBOI_SOME_ERROR(errval))
		{
			error ("failed setting option %s to %s (error %d)!",os.c_str(),vs.c_str(),errval);
			goto err;
		}
		goto ret;
	}

	if( nargin >= 2 && args(0).is_string() && args(0).string_value()=="set" /* && args(1).is_string() */ )
	{
		// sparsersb ("set", XXX)
		error ("%s", "did you intend to set librsb options ? use the correct syntax then ! (third argument missing)"); goto errp;
	}

	if( nargin == 2 && args(0).is_string() && args(0).string_value()=="get" && args(1).is_string() )
	{
		// sparsersb ("get", XXX)
		/* FIXME: unfinished feature ! */
		RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
		error ("%s", "getting library options still unimplemented!");
		goto errp;
	}

#if defined(RSB_LIBRSB_VER) && (RSB_LIBRSB_VER>=10100)
	if (nargin >= 2 && isr && args(1).is_string() && args(1).string_value()=="autotune")
	{
		// sparsersb (S,"autotune"[, TRANSA, NRHS, MAXR, TMAX, TN, SF])
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		/* these are user settable */
		rsb_coo_idx_t nrhs = 0;
		rsb_int_t maxr = 1;
		rsb_time_t tmax = 2.0;
		rsb_int_t tn = 0;
		rsb_real_t sf = 1.0;
		rsb_trans_t transA = RSB_TRANSPOSITION_N;
		/* the following may also be user settable in the future */
		const void *alphap = RSBOI_NULL;
		const void *betap = RSBOI_NULL;
		/* these not */
	       	rsb_flags_t order = RSB_OI_DMTXORDER;
	       	const void * Bp = RSBOI_NULL;
		rsb_nnz_idx_t ldB = 0;
		rsb_nnz_idx_t ldC = 0;
		void * Cp = RSBOI_NULL;

		if (nargin > 2) transA = RSB_CHAR_AS_TRANSPOSITION(args(2).string_value()[0]);
		if (transA == RSBOI_INVALID_TRANS_CHAR)
		{
			RSBOI_0_ERROR(RSBOI_0_WTRANSMSG);
			goto ret;
		}
		if (nargin > 3) nrhs = args(3).scalar_value();
		if (nargin > 4) maxr = args(4).scalar_value();
		if (nargin > 5) tmax = args(5).scalar_value();
		if (nargin > 6) tn = args(6).scalar_value();
		if (nargin > 7) sf = args(7).scalar_value();

		// ...
		if(!osmp || !osmp->mtxAp)
			RSBOI_0_INTERRMSGSTMT(goto ret)
		if(nargout)
		{
			struct rsb_mtx_t *mtxAp = RSBOI_NULL;
			errval = rsb_mtx_clone(&mtxAp,RSB_NUMERICAL_TYPE_SAME_TYPE,RSB_TRANSPOSITION_N,RSBOI_NULL,osmp->mtxAp,RSBOI_EXPF);
			rsboi_error(errval);
			errval = rsb_tune_spmm(&mtxAp,&sf,&tn,maxr,tmax,transA,alphap,RSBOI_NULL,nrhs,order,Bp,ldB,betap,Cp,ldC);
			rsboi_error(errval);
			retval.append(new octave_sparsersb_mtx(mtxAp));
		}
		else
			errval = rsb_tune_spmm(&osmp->mtxAp,&sf,&tn,maxr,tmax,transA,alphap,RSBOI_NULL/*osmp->mtxAp*/,nrhs,order,Bp,ldB,betap,Cp,ldC);
		rsboi_error(errval);
		goto ret;
	}
#endif


#if defined(RSB_LIBRSB_VER) && (RSB_LIBRSB_VER>=10100)
	if (nargin >= 3 && isr
 		&& args(1).is_string() && args(1).string_value().substr(0,6)=="render"
		&& args(2).is_string())
	{
		// sparsersb (S,"render", FILENAME[, RWIDTH, RHEIGHT])
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		std::string rmf = args(2).string_value();
		rsb_coo_idx_t pmWidth = 512, pmHeight = 512; /* Care to update the documentation when changing these. */
		rsb_flags_t marf = RSB_MARF_EPS;
		/* may tell the user to supply a sparsersb matrix in case input is not 'sparse' */

		if (nargin > 3) pmWidth = args(3).scalar_value();
		if (nargin > 4) pmHeight = args(4).scalar_value();

		if(!osmp || !osmp->mtxAp)
			RSBOI_0_INTERRMSGSTMT(goto ret)

 		if( args(1).string_value() == "renders")
			marf = RSB_MARF_EPS_S;
 		if( args(1).string_value() == "renderb")
			marf = RSB_MARF_EPS_B;
		errval = rsb_mtx_rndr(rmf.c_str(),osmp->mtxAp,pmWidth,pmHeight,marf);
		if(RSBOI_SOME_ERROR(errval))
		{
			retval.append(std::string("Error returned from rsb_mtx_rndr()"));
			rsboi_error(errval);
		}
		goto ret;
	}
#endif
#if RSBOI_WANT_MTX_SAVE
	if (nargin == 3 && isr
 		&& args(1).is_string() && args(1).string_value()=="save"
		&& args(2).is_string())
	{
		// sparsersb (A,"save",MTXFILENAME)
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		errval = rsb_file_mtx_save(osmp->mtxAp,args(2).string_value().c_str());
		rsboi_error(errval);
		goto ret;
	}
#endif
	if (nargin == 3 && isr
 		&& args(1).is_string() && args(1).string_value()=="get"
		&& args(2).is_string())
	{
		// sparsersb (S, "get", MIF)
		// For any version of lirsb, you can get valid values with e.g.:
		// grep RSB_MIF path-to/rsb.h | sed 's/^[, ]*//g;s/\([A-Z_]\+\).*<\(.\+\)(.*$/\1: \2/g;s/$/;/g'
		rsb_err_t errval = RSB_ERR_NO_ERROR;
		char is[RSBOI_INFOBUF];
		char ss[RSBOI_INFOBUF];

		if(!osmp || !osmp->mtxAp)
			RSBOI_0_INTERRMSGSTMT(goto ret)

		if(strlen(args(2).string_value().c_str())==0)
			strncpy(is,"RSB_MIF_MATRIX_INFO__TO__CHAR_P",sizeof(is));
		else
			strncpy(is,args(2).string_value().c_str(),sizeof(is));
		errval = rsb_mtx_get_info_str(osmp->mtxAp,is,ss,RSBOI_INFOBUF);

		if(!RSBOI_SOME_ERROR(errval))
		{
			retval.append(octave_value(ss));
			goto ret;
		}
		if(RSBOI_SOME_ERROR(errval))
		{
			retval.append(std::string("Error returned from rsb_mtx_get_info_from_string()"));
		}
		goto ret;
	}

	if ( nargin >= 3 && isr && args(1).is_string() && args(1).string_value()=="get" /* && args(1).is_string() */ )
	{
		// sparsersb (S, "get", MIF, XXX)
		error ("%s", "did you intend to get matrices information ? use the correct syntax then !");
		goto errp;
	}

	if ( nargin == 1 || nargin == 2 )
	{
		rsb_type_t typecode = RSBOI_TYPECODE;
		if (nargin >= 2)/* FIXME: this is weird ! */
#if RSBOI_WANT_DOUBLE_COMPLEX
			typecode = RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX;
#else /* RSBOI_WANT_DOUBLE_COMPLEX */
			RSBOI_0_ERROR(RSBOI_0_NOCOERRMSG);
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */

		if (nargin == 2 && isr && args(1).is_string())
#if RSBOI_WANT_QSI
		{
			// sparsersb (S, QS)
			char ss[RSBOI_INFOBUF];
			rsb_err_t errval = RSB_ERR_NO_ERROR;

			if(!osmp || !osmp->mtxAp)
				RSBOI_0_INTERRMSGSTMT(goto ret)
			errval = rsb_mtx_get_info_str(osmp->mtxAp,"RSB_MIF_MATRIX_INFO__TO__CHAR_P",ss,RSBOI_INFOBUF);
			if(!RSBOI_SOME_ERROR(errval))
				retval.append(ss);
			/* TODO, FIXME: to add interpretation (we are ignoring args(1) !): this is to be extended. */
			RSBOI_WARN(RSBOI_0_UNFFEMSG);/* FIXME: this is yet unfinished */
			// octave_stdout << "Matrix information (in the future, supplementary information may be returned, as more inquiry functionality will be implemented):\n" << ss << "\n";
			/* FIXME: shall not print out, but rather return the info as a string*/
			//retval.append("place info string here !\n");
			goto ret;
		}
#else /* RSBOI_WANT_QSI */
		{
			// sparsersb (S, QS)
			error ("%s", "invocation error !");
		       	goto errp;
		}
#endif /* RSBOI_WANT_QSI */
		else
		if(args(0).issparse())
		{
			// sparsersb (sparse(...), ...)
			if( isr )
			{
				RSBOI_WARN(RSBOI_0_UNFFEMSG);
				retval.append(osmp = (octave_sparsersb_mtx*)(args(0).get_rep()).clone());
			}
			else
			{
				if(!ic0)
				{
					const SparseMatrix m = args(0).sparse_matrix_value();
					RSBOI_IF_ERR( goto err;)
					retval.append(osmp = new octave_sparsersb_mtx(m,typecode));
				}
#if RSBOI_WANT_DOUBLE_COMPLEX
				else
				{
					const SparseComplexMatrix m = args(0).sparse_complex_matrix_value();
					RSBOI_IF_ERR( goto err;)
					retval.append(osmp = new octave_sparsersb_mtx(m,typecode));
				}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
			}
		}
		else
		if(args(0).is_string())
		{
		RSBOI_TRY_BLK
		{
			// sparsersb (MTXFILENAME)
			const std::string mtxfilename = args(0).string_value();
			RSBOI_IF_ERR( goto err;)
			if(mtxfilename == RSBOI_LIS)
			{
				//retval.append(RSB_NUMERICAL_TYPE_PREPROCESSOR_SYMBOLS);
#if RSBOI_WANT_DOUBLE_COMPLEX
				retval.append("D Z");
#else /* RSBOI_WANT_DOUBLE_COMPLEX */
				retval.append("D");
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
				goto ret;
			}
			else
			{
				// [S, NROWS, NCOLS, NNZ, REPINFO, FIELD, SYMMETRY] = sparsersb (MTXFILENAME)
				rsb_type_t typecode = RSBOI_TYPECODE;
				RSBOI_WARN(RSBOI_0_UNFFEMSG);
				RSBOI_WARN("shall set the type, here");
				if(nargin>1 && args(1).is_string())
				{
					const std::string mtxtypestring = args(1).string_value();
					if(mtxtypestring == "complex" || mtxtypestring == "Z")
#if RSBOI_WANT_DOUBLE_COMPLEX
						typecode = RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX;
#else
						RSBOI_0_ERROR(RSBOI_0_NOCOERRMSG);
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
					if(mtxtypestring == "real" || mtxtypestring=="D")
						typecode = RSB_NUMERICAL_TYPE_DOUBLE;
				}
#if RSBOI_WANT_MTX_LOAD
				osmp = new octave_sparsersb_mtx(mtxfilename,typecode);
#else /* RSBOI_WANT_DOUBLE_COMPLEX */
				goto ret; /* TODO: need error message here */
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
				if(osmp->mtxAp)
					retval.append(osmp);
				else
					delete osmp;
#if RSBOI_WANT_VECLOAD_INSTEAD_MTX
				if(!osmp->mtxAp)
                		{
					rsb_nnz_idx_t n = 0;
					rsb_file_vec_load(mtxfilename.c_str(),typecode,RSBOI_NULL,&n);
					if(n<1)
					{
						// error ("%s", "are you sure you passed a valid Matrix Market vector file ?");
						goto err;
					}

					if(typecode == RSB_NUMERICAL_TYPE_DOUBLE)
					{
						Matrix retvec(n,1,RSBOI_ZERO);
						rsb_file_vec_load(mtxfilename.c_str(),typecode,(RSBOI_T*)retvec.data(),&n);
						retval.append(retvec);
					}
#if RSBOI_WANT_DOUBLE_COMPLEX
					else
					if(typecode == RSB_NUMERICAL_TYPE_DOUBLE_COMPLEX)
					{
						ComplexMatrix retvec(n,1,RSBOI_ZERO);
						rsb_file_vec_load(mtxfilename.c_str(),typecode,(RSBOI_T*)retvec.data(),&n);
						retval.append(retvec);
					}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
					goto ret;
				}
#endif
				if(nargout) nargout--;
				if(nargout) retval.append(osmp->rows()),--nargout;
				if(nargout) retval.append(osmp->cols()),--nargout;
				if(nargout) retval.append(osmp->nnz()),--nargout;
				if(nargout) retval.append(osmp->get_info_string()),--nargout;
				if(nargout) retval.append((!osmp->iscomplex())?"real":"complex"),--nargout;
				if(nargout) retval.append(osmp->get_symmetry()),--nargout;
			}
		}
		RSBOI_CATCH_BLK
		}
		else
		{
		RSBOI_TRY_BLK
		{
			if (nargin == 2  && args(0).is_scalar_type() && args(1).is_scalar_type() )
			{
				// sparsersb (M, N)
				const SparseMatrix m = args(0).sparse_matrix_value();
				retval.append(osmp = new octave_sparsersb_mtx(SparseMatrix(args(0).scalar_value(),args(1).scalar_value())));
			}
			else
			{
				// sparsersb (A, XXX)
				if(!ic0)
				{
					Matrix m = args(0).matrix_value();
					RSBOI_IF_ERR( goto err;)
					retval.append(osmp = new octave_sparsersb_mtx(m));
				}
#if RSBOI_WANT_DOUBLE_COMPLEX
				else
				{
					ComplexMatrix m = args(0).complex_matrix_value();
					RSBOI_IF_ERR( goto err;)
					retval.append(osmp = new octave_sparsersb_mtx(m));
				}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
				if(nargin >= 2)
				{ error ("%s", "when initializing from a single matrix, no need for second argument !"); goto errp; }
			}
		}
		RSBOI_CATCH_BLK
		}
	}
	else
	if (nargin >= 3 && nargin <= 7 && !(args(0).is_string() || args(1).is_string() || args(2).is_string() ) )
	{
		// sparsersb (I, J, SV, M, N, "unique")
		rsb_flags_t eflags = RSBOI_DCF;
		rsb_flags_t sflags = RSB_FLAG_NOFLAGS;
		octave_idx_type nrA = 0, ncA = 0;
		int sai = 0; // string argument index

		if (nargin > 3)
		{
			if ( nargin < 5)
			{
				if(nargin == 4 && args(3).is_string())
					goto checked;
				RSBOI_EERROR(RSBOI_0_BADINVOERRMSG);
				goto errp;
			}
			/* FIXME: integer_type should be also supported here: shouldn't it ?*/
    			if( (!args(3).is_scalar_type()) || (!args(4).is_scalar_type()))
			{
				RSBOI_EERROR(RSBOI_0_BADINVOERRMSG);
				goto errp;
			}
     			if( nargin > 5 && ((!args(5).is_string()) && (!args(5).is_scalar_type())))
			{
				RSBOI_EERROR(RSBOI_0_BADINVOERRMSG);
				goto errp;
			}
		}
checked:
		if (nargin >= 5  )
		{
			nrA = args(3).scalar_value();/* FIXME: need index value here! */
			ncA = args(4).scalar_value();
			if(nrA<=0 || ncA<=0)
			{
				RSBOI_EERROR(RSBOI_O_NPMSERR);
				goto errp;
			}
		}

		if (nargin >= 6  && args(5).is_string())
			sai = 5;
		else
			if (nargin == 4  && args(3).is_string())
				sai = 3;
		for(;sai>0 && sai<nargin;++sai)
		{
			std::string vv = args(sai).string_value();

			if ( vv == "summation" || vv == "sum" )
				eflags = RSB_FLAG_DUPLICATES_SUM;
			else
			if ( vv == "unique" )
				eflags = RSB_FLAG_DUPLICATES_KEEP_LAST;
#if RSBOI_WANT_SYMMETRY
			else
			if ( vv == "symmetric" || vv == "sym" )
				sflags = RSB_FLAG_SYMMETRIC;
			else
			if ( vv == "hermitian" || vv == "her" )
				sflags = RSB_FLAG_HERMITIAN;
			else
			if ( vv == "general" || vv == "gen" )
				;
#endif /* RSBOI_WANT_SYMMETRY */
			else
			{
				vv = "'" + vv;
				vv+="' is not a recognized keyword (unlike 'summation', 'unique', 'symmetric', 'hermitian', 'general')!";
				error ("%s",vv.c_str());
				goto errp;
			}
		}
		RSB_DO_FLAG_ADD(eflags,sflags);
		if (nargin >= 6  && args(5).isinteger())
		{
			/* we ignore this value for MATLAB compatibility */
		}

		RSBOI_IF_ERR( goto err;)

		if(!ic3)
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			idx_vector iv = args(0).index_vector ();
			idx_vector jv = args(1).index_vector ();
			retval.append(osmp = new octave_sparsersb_mtx( iv, jv, args(2).matrix_value(),nrA,ncA,eflags ));
		}
#if RSBOI_WANT_DOUBLE_COMPLEX
		else
		{
			RSBOI_DEBUG_NOTICE(RSBOI_D_EMPTY_MSG);
			idx_vector iv = args(0).index_vector ();
			idx_vector jv = args(1).index_vector ();
			retval.append(osmp = new octave_sparsersb_mtx( iv, jv, args(2).complex_matrix_value(),nrA,ncA,eflags ));
		}
#endif /* RSBOI_WANT_DOUBLE_COMPLEX */
	}
	else
		goto errp;
	if(!osmp)
	{
		RSBOI_WARN(RSBOI_0_NEEDERR);
		RSBOI_DEBUG_NOTICE(RSBOI_0_FATALNBMSG);
	}
#if RSBOI_WANT_HEAVY_DEBUG
	if(!rsb_is_correctly_built_rcsr_matrix(osmp->mtxAp)) // function non in rsb.h's API
	{
		RSBOI_WARN(RSBOI_0_NEEDERR);
		RSBOI_DEBUG_NOTICE(RSBOI_0_UNCBERR);
	}
#endif
	goto err;
	errp:
	print_usage ();
err:
ret:
	return retval;
}
/*
%!test
%! assert( 0==nnz(sparsersb(3,3) .- sparse(3,3)) )
%!test
%! assert( 0==nnz(sparsersb([],[],[],3,3    ) .- sparse([],[],[],3,3    )) )
%!test
%! assert( 0==nnz(sparsersb([],[],[],3,3,123) .- sparse([],[],[],3,3,321)) )
%!test
%! s=sparsersb([2]);
%! assert(s==2);
%! assert(s!=1)
%!test
%! s=sparsersb([1,2],[1,1],[11,21],2,2         );
%! assert(nnz(s)==2)
%!test
%! s=sparsersb([1,2],[1,1],[11,21],2,2,-1      );
%! assert(nnz(s)==2)
%!test
%! s=sparsersb([1,2],[1,1],[11,21]             );
%! assert(nnz(s)==2)
%!test
%! s=sparsersb(10,10                           );
%! assert(nnz(s)==0)
%!test
%! s=sparsersb([1,1],[1,1],[11,21]             );
%! assert(nnz(s)==1)
%! assert(s(1,1)==32)
%!test
%! s=sparsersb([1,1],[1,1],[11,21],2,2,"unique");
%! assert(nnz(s)==1),
%! assert(s(1,1)==21)
%!test
%! sparsersb("set","RSB_IO_WANT_VERBOSE_TUNING","1");
%!test
%! sparsersb("set","RSB_IO_WANT_VERBOSE_TUNING","0");
%!test
%! wvt=0;
%! try
%! sparsersb("set","...")
%! wvt=1;
%! end_try_catch
%! assert(wvt==0)
%!test
%! sparsersb("set","FIXME: WE UNFORTUNATELY STILL SILENTLY IGNORE ERRORS HERE (NO RETURN VALUE)","1");
%!test
%! wvt=-1;
%! try
%! wvt=sparsersb("get","RSB_IO_WANT_VERBOSE_TUNING")
%! assert(wvt==0 || wvt ==1)
%! end_try_catch
%! assert(wvt==-1)
%!test
%! s=sparsersb([1]);
%! assert(sparsersb(s,"get","RSB_MIF_LEAVES_COUNT__TO__RSB_BLK_INDEX_T")=="1")
%!test
%! s=sparsersb([1]);
%! assert(str2num(sparsersb(s,"get","RSB_MIF_LEAVES_COUNT__TO__RSB_BLK_INDEX_T"))==1)
%!test
%! s=sparsersb([1]);
%! assert(str2num(sparsersb(s,"get","RSB_MIF_INDEX_STORAGE_IN_BYTES__TO__SIZE_T"))>1)
%!test
%! s=sparsersb([1]);
%! assert(str2num(sparsersb(s,"get","RSB_MIF_INDEX_STORAGE_IN_BYTES_PER_NNZ__TO__RSB_REAL_T"))>1)
%!test
%! s=sparsersb([1]);
%! assert(str2num(sparsersb(1*s,"get","RSB_MIF_MATRIX_TYPECODE__TO__RSB_TYPE_T"))==68) # D
%!test
%! s=sparsersb([1]);
%! assert(str2num(sparsersb(i*s,"get","RSB_MIF_MATRIX_TYPECODE__TO__RSB_TYPE_T"))==90) # Z
%!test
%! s=sparsersb([1,1],[1,1],[11,21],2,2,"unique");
%! assert(str2num(sparsersb(s,"get","RSB_MIF_LEAVES_COUNT__TO__RSB_BLK_INDEX_T"))>0)
%!test
%! s=sparsersb([1]);
%! v=1;
%! assert(strfind(sparsersb(sparsersb([1]),"get","WRONG SPEC STRING"),"Error")==1)
%!test
%! assert(sparsersb(sparsersb([11,0;21,22]),"get","RSB_MIF_TOTAL_SIZE__TO__SIZE_T")>1)
%!test
%! assert(length(sparsersb(sparsersb([11,0;21,22]),"RSB_MIF_TOTAL_SIZE__TO__SIZE_T"))>1)
%!test
%! s=sparsersb([11,0;21,22]);
%! assert(sparsersb(s,"RSB_MIF_TOTAL_SIZE__TO__SIZE_T") == sparsersb(s,"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"))
%!test
%! s=sparsersb([1]);
%! sparsersb(sparsersb([11,0;21,22]),"save","sparsersb_temporary_matrix_file.mtx")
%!test
%! [S, NROWS, NCOLS, NNZ, REPINFO, FIELD, SYMMETRY] = sparsersb("sparsersb_temporary_matrix_file.mtx"     );
%! assert(NROWS==2)
%! assert(NCOLS==2)
%! assert(NNZ==3)
%! assert(FIELD=="real");
%! assert(SYMMETRY=='U');
%!test
%! [S, NROWS, NCOLS, NNZ, REPINFO, FIELD, SYMMETRY] = sparsersb("sparsersb_temporary_matrix_file.mtx", "Z");
%! assert(NROWS==2);
%! assert(NCOLS==2);
%! assert(NNZ==3);
%! assert(FIELD=="complex");
%! assert(SYMMETRY=='U');
%!test
%! [S, NROWS, NCOLS, NNZ, REPINFO, FIELD] = sparsersb("sparsersb_temporary_matrix_file.mtx", "D");
%! assert(NROWS==2);
%! assert(NCOLS==2);
%! assert(NNZ==3);
%! assert(FIELD=="real");
%!test
%! [S, NROWS, NCOLS, NNZ, REPINFO] = sparsersb("sparsersb_temporary_matrix_file.mtx", "D");
%! assert(NROWS==2);
%! assert(NCOLS==2);
%! assert(NNZ==3);
%!test
%! [S, NROWS, NCOLS] = sparsersb("sparsersb_temporary_matrix_file.mtx", "D");
%! assert(NROWS==2);
%! assert(NCOLS==2);
%!test
%! [S, NROWS] = sparsersb("sparsersb_temporary_matrix_file.mtx", "D");
%! assert(NROWS==2);
%!test
%! rrm=sparsersb(sprand(1000,1000,0.001));
%! sparsersb(rrm,"render", "sparsersb_temporary_render.eps" ,1024); # will use defaults for rWidth
%!test
%! rrm=sparsersb(sprand(1000,1000,0.001));
%! sparsersb(rrm,"render", "sparsersb_temporary_render.eps" ,1024,1024);
%! # sparsersb(rrm,"renderb", "sparsersb_temporary_renderb.eps"); sparsersb(rrm,"renders", "sparsersb_temporary_renders.eps"); # FIXME
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! sparsersb(a,"autotune");
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! o=sparsersb(a,"AUTOTUNE");
%! v=0;
%! try
%!   assert(o==a)
%!   v=1
%! end_try_catch
%! assert(v==0)
%! assert(length(o)>10)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! o=sparsersb(a,"autotune");
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=2;
%! o=sparsersb(a,"autotune","n",nrhs);
%! assert(o==a)
%!test
%! a=1*sparsersb(sprand(100,100,0.4));
%! nrhs=2;
%! o=sparsersb(a,"autotune","c",nrhs);
%! assert(o==a)
%!test
%! a=i*sparsersb(sprand(100,100,0.4));
%! nrhs=2;
%! o=sparsersb(a,"autotune","c",nrhs);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=1;
%! maxr=1;
%! o=sparsersb(a,"autotune","N",nrhs,maxr);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=1;
%! maxr=1;
%! tmax=1;
%! o=sparsersb(a,"autotune","n",nrhs,maxr,tmax);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=1;
%! maxr=1;
%! tmax=1;
%! tn=1;
%! o=sparsersb(a,"autotune","n",nrhs,maxr,tmax,tn);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=1;
%! maxr=1;
%! tmax=1;
%! tn=1;
%! sf=1;
%! o=sparsersb(a,"autotune","n",nrhs,maxr,tmax,tn,sf);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=1;
%! maxr=1;
%! tmax=1;
%! tn=2;
%! sf=1;
%! if getenv ("OMP_NUM_THREADS") != "1" ;
%!   o=sparsersb(a,"autotune","n",nrhs,maxr,tmax,tn,sf);
%!   assert(o==a)
%! end
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=20;
%! maxr=1;
%! tmax=1;
%! tn=1;
%! o=sparsersb(a,"autotune","t",nrhs,maxr,tmax,tn);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=20;
%! maxr=1;
%! tmax=1;
%! tn=1;
%! sf=1;
%! o=sparsersb(a,"autotune","n",nrhs,maxr,tmax,tn,sf);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=20;
%! maxr=1;
%! tmax=1;
%! tn=0;
%! sf=1;
%! o=sparsersb(a,"autotune","n",nrhs,maxr,tmax,tn,sf);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=20;
%! maxr=0;
%! tmax=0;
%! tn=0;
%! sf=1;
%! o=sparsersb(a,"autotune","n",nrhs,maxr,tmax,tn,sf);
%! assert(o==a)
%!test
%! a=sparsersb(sprand(100,100,0.4));
%! nrhs=1;
%! maxr=0;
%! tmax=0;
%! tn=0;
%! sf=1;
%! wvt=0;
%! try
%! o=sparsersb(a,"autotune","?",nrhs,maxr,tmax,tn,sf);
%! wvt=1;
%! end_try_catch
%! assert(wvt==0)
%! # assert(o==a) # o undefined
%!test
%! assert( nnz(sparse((toeplitz(sparsersb([0,1,2,3]))-toeplitz(sparse([0,1,2,3])))))==0 );
%!test
%! assert( prod(sparsersb([11,12,13;21,22,23])(:) - [11,21,12,22,13,23]')==0);
%!test
%! A = sprand(4,4,.5);
%! assert(prod(reshape(sparsersb(A),[8,2]) - reshape(sparse(A),[8,2]))==0);
%!test
%! assert(sparsersb([-1,1,2])() == sparsersb([-1,1,2]));
%!test
%! % symmetry expansion
%! A=sparsersb([1+i,0,1;0,1,0;1,0,1]);
%! assert(nnz(A)==4 && nnz(full(A))==5);
%!test
%! % 1-D indexing access is meant to be like in sparse
%! A=sparsersb([1+i,0,1;0,1,0;1,0,1]);
%! assert(A(1)==(1+i) && A(3)==1 && sparse(A)(3)==1)
%! A=sparsersb([1+i,0,1;0,1,0;1,0,1]);
%! assert(A(1)==(1+i) && A(2)==0 && sparse(A)(2)==0)
%!test
%! A=sparsersb([1+i,0,1;0,1,0;1,0,1]);
%! assert(0==A(2:5)-sparsersb([1,1],[2,4],[1+0i,1+0i],1,4))
%!test
%! assert(  nnz(sparse([2,1;1,2])) == 4 && nnz(sparsersb([2,1;1,2])) == 3  )    #    symmetry
%! assert(  nnz(sparse([2,0;1,2])) == 3 && nnz(sparsersb([2,0;1,2])) == 3  )    # no symmetry
%!test
%! assert( (sparse([2,0;1,2]) \ [1;1])  == (sparsersb([2,0;1,2]) \ [1;1])  )
%! assert( (sparse([2,0;0,2]) \ [1;1])  == (sparsersb([2,0;0,2]) \ [1;1])  )
%!test
%! assert( (sparse([2,0;1,2]) * [1;1])  == (sparsersb([2,0;1,2]) * [1;1])  )
%! assert( (sparse([2,0;0,2]) * [1;1])  == (sparsersb([2,0;0,2]) * [1;1])  )
%!test
%! % no symmetry expansion and no conversion:
%! assert ( ( sparsersb([1,1;1,1])*sparsersb([1,1;1,1] )) == ( sparse([1,0;1,1])*sparse([1,0;1,1]) ) )
%!test
%! % symmetry expansion and sparsersb->sparse conversion:
%! assert ( ( sparsersb([1,1;1,1])*sparse   ([1,1;1,1] )) == ( sparse([1,1;1,1])*sparse([1,1;1,1]) ) )
%!test
%! % symmetry expansion and sparsersb->sparse conversion:
%! assert ( ( sparse   ([1,1;1,1])*sparsersb([1,1;1,1] )) == ( sparse([1,1;1,1])*sparse([1,1;1,1]) ) )
%!test
%! % no symmetry expansion and no conversion:
%! assert ( ( sparsersb([1,1;1,1])+sparsersb([1,1;1,1] )) == ( sparse([1,0;1,1])+sparse([1,0;1,1]) ) )
%!test
%! % symmetry expansion and sparsersb->sparse conversion:
%! assert ( ( sparsersb([1,1;1,1])+sparse   ([1,1;1,1] )) == ( sparse([1,1;1,1])+sparse([1,1;1,1]) ) )
%!test
%! % symmetry expansion and sparsersb->sparse conversion:
%! assert ( ( sparse   ([1,1;1,1])+sparsersb([1,1;1,1] )) == ( sparse([1,1;1,1])+sparse([1,1;1,1]) ) )
%!test
%! % symmetry specification: general
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1,1],"general") == sparse([1,0;1,1]) )
%!test
%! % symmetry specification: symmetric
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1,1],"symmetric") == sparse([1,1;1,1]) )
%!test
%! % symmetry specification: symmetric, and conversion and (:,:) access
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"symmetric") == sparse([1,1+i;1+i,1]) )
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"symmetric") == sparsersb([1,2,2],[1,1,2],[1,1+i,1],"symmetric")(:,:) )
%!test
%! % symmetry specification: hermitian, and conversion and (:,:) access
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1,1],"hermitian") == sparse([1,1;1,1]) )
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1,1],"hermitian") == sparsersb([1,2,2],[1,1,2],[1,1,1],"hermitian")(:,:) )
%!test
%! % symmetry specification: hermitian, and conversion and (:,:) access
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"hermitian") == sparse([1,1-i;1+i,1]) )
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"hermitian") == sparsersb([1,2,2],[1,1,2],[1,1+i,1],"hermitian")(:,:) )
%!test
%! % symmetry or hermitianness: no empty triangle access
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"symmetric")(1,2) == 0 )
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"hermitian")(1,2) == 0 )
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"general")(1,2) == 0 )
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"symmetric")(2,1) != 0 )
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"hermitian")(2,1) != 0 )
%! assert ( sparsersb([1,2,2],[1,1,2],[1,1+i,1],"general")(2,1) != 0 )
%!test
%! % symmetry expansion
%! assert ( nnz(sparse(sparsersb([1  ,1;1,1])) - sparse([1  ,1;1,1])) == 0 )
%! assert ( nnz(sparse(sparsersb([1+i,1;1,1])) - sparse([1+i,1;1,1])) == 0 )
%! % hermitianness expansion
%! assert ( nnz(sparsersb([1,1+i;1-i,1]) - sparse([1,1+i;1-i,1])) == 0 )
%! % no symmetric complex expansion
%! assert ( nnz(sparsersb([1,1+i;1+i,1]) - sparse([1,1+i;1-i,1])) == 1 )
%!test
%! assert( (sparse([2, 0; 1, 2])(:,:))  == (sparsersb([2, 0; 1, 2])(:,:))  )
%! assert( (sparse([2, 0; i, 2])(:,:))  == (sparsersb([2, 0; i, 2])(:,:))  )
%!test
%! assert( (sparse([2, 0; 1, 2])(2,:))  == (sparsersb([2, 0; 1, 2])(2,:))  )
%! assert( (sparse([2, 0; i, 2])(2,:))  == (sparsersb([2, 0; i, 2])(2,:))  )
%!test
%! assert( (sparse([2, 0; 1, 2])(:,2))  == (sparsersb([2, 0; 1, 2])(:,2))  )
%! assert( (sparse([2, 0; i, 2])(:,2))  == (sparsersb([2, 0; i, 2])(:,2))  )
%!test
%! assert( (sparse([2,0;1,2]) * [1;1])  == (sparsersb([2,0;1,2]) * [1;1])  )
%! assert( (sparse([2,0;0,2]) * [1;1])  == (sparsersb([2,0;0,2]) * [1;1])  )
%!test
%! assert( (sparse([2,0;1,2]) * [i;1])  == (sparsersb([2,0;1,2]) * [i;1])  )
%! assert( (sparse([2,0;0,2]) * [i;1])  == (sparsersb([2,0;0,2]) * [i;1])  )
%!test
%! assert( (sparse([2,0;1,2])'* [1;1])  == (sparsersb([2,0;1,2])'* [1;1])  )
%! assert( (sparse([2,0;0,2])'* [1;1])  == (sparsersb([2,0;0,2])'* [1;1])  )
%!test
%! assert( (sparse([2,0;1,2])'* [i;1])  == (sparsersb([2,0;1,2])'* [i;1])  )
%! assert( (sparse([2,0;0,2])'* [i;1])  == (sparsersb([2,0;0,2])'* [i;1])  )
%!test
%! assert( (sparse([2,0;1,2]).'* [1;1])  == (sparsersb([2,0;1,2]).'* [1;1])  )
%! assert( (sparse([2,0;0,2]).'* [1;1])  == (sparsersb([2,0;0,2]).'* [1;1])  )
%!test
%! assert( (sparse([2,0;1,2]).'* [i;1])  == (sparsersb([2,0;1,2]).'* [i;1])  )
%! assert( (sparse([2,0;0,2]).'* [i;1])  == (sparsersb([2,0;0,2]).'* [i;1])  )
%!test
%! assert( (sparse([2,0;1,2]) *  1   )  == (sparsersb([2,0;1,2]) *  1   )  )
%! assert( (sparse([2,0;0,2]) *  1   )  == (sparsersb([2,0;0,2]) *  1   )  )
%!test
%! assert( (sparse([2,0;1,2]) *  i   )  == (sparsersb([2,0;1,2]) *  i   )  )
%! assert( (sparse([2,0;0,2]) *  i   )  == (sparsersb([2,0;0,2]) *  i   )  )
%!test
%! assert( (sparse([2,0;1,2]).*  1   )  == (sparsersb([2,0;1,2]).*  1   )  )
%! assert( (sparse([2,0;0,2]).*  1   )  == (sparsersb([2,0;0,2]).*  1   )  )
%!test
%! assert( (sparse([2,0;1,2]).*  i   )  == (sparsersb([2,0;1,2]).*  i   )  )
%! assert( (sparse([2,0;0,2]).*  i   )  == (sparsersb([2,0;0,2]).*  i   )  )
%!test
%! assert( ( sparse([2,0;1,2]) + 1*sparse([2,0;1,2]) )  == (sparsersb([2,0;1,2]) + 1*sparsersb([2,0;1,2]) )  )
%!test
%! assert( ( sparse([2,0;0,2]) + 1*sparse([2,0;0,2]) )  == (sparsersb([2,0;0,2]) + 1*sparsersb([2,0;0,2]) )  )
%!test
%! assert( ( sparse([2,0;1,2]) + i*sparse([2,0;1,2]) )  == (sparsersb([2,0;1,2]) + i*sparsersb([2,0;1,2]) )  )
%!test
%! assert( ( sparse([2,0;0,2]) + i*sparse([2,0;0,2]) )  == (sparsersb([2,0;0,2]) + i*sparsersb([2,0;0,2]) )  )
%!test
%! assert( (sparse([ 1 + 1i,0;0, 1 + 1i]).'* [1,2;1,2]) == (sparsersb([ 1 + 1i,0;0, 1 + 1i]).'* [1,2;1,2]) )
*/


/* GENERATED TEST LINES BEGIN */ /*

%% tests for a 1 x 1 matrix,  density 10%, real
%!test
%! A = [0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (reshape (sparsersb (A), 1, 1) == reshape (sparse (A), 1, 1));

%% tests for a 1 x 1 matrix,  density 10%, complex
%!test
%! A = [0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (reshape (sparsersb (A), 1, 1) == reshape (sparse (A), 1, 1));

%% tests for a 1 x 3 matrix,  density 10%, real
%!test
%! A = [0,0,0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,3) == sparse (A)(1,3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,0,0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,0,0,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 1 x 3 matrix,  density 10%, complex
%!test
%! A = [0,0,0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,3) == sparse (A)(1,3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,0,0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,0,0,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 1 matrix,  density 10%, real
%!test
%! A = [0,;0,;0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3,1) == sparse (A)(3,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,;0,;0,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 1 matrix,  density 10%, complex
%!test
%! A = [0,;0,;0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3,1) == sparse (A)(3,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,;0,;0,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 3 matrix,  density 10%, real
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)(3,3) == sparse (A)(3,3));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)(9) == sparse (A)(9));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (reshape (sparsersb (A), 1, 9) == reshape (sparse (A), 1, 9));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (reshape (sparsersb (A), 3, 3) == reshape (sparse (A), 3, 3));
%!test
%! A = [0,0,0,;0,0,0,;0,3,0,;]; assert (reshape (sparsersb (A), 9, 1) == reshape (sparse (A), 9, 1));

%% tests for a 3 x 3 matrix,  density 10%, complex
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)(3,3) == sparse (A)(3,3));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)(9) == sparse (A)(9));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (reshape (sparsersb (A), 1, 9) == reshape (sparse (A), 1, 9));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (reshape (sparsersb (A), 3, 3) == reshape (sparse (A), 3, 3));
%!test
%! A = [0,0,0,;0,0,0,;0,0,0,;]; assert (reshape (sparsersb (A), 9, 1) == reshape (sparse (A), 9, 1));

%% tests for a 1 x 1 matrix,  density 20%, real
%!test
%! A = [0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (reshape (sparsersb (A), 1, 1) == reshape (sparse (A), 1, 1));

%% tests for a 1 x 1 matrix,  density 20%, complex
%!test
%! A = [0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (reshape (sparsersb (A), 1, 1) == reshape (sparse (A), 1, 1));

%% tests for a 1 x 3 matrix,  density 20%, real
%!test
%! A = [22,0,0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)(1,3) == sparse (A)(1,3));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [22,0,0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [22,0,0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [22,0,0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [22,0,0,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [22,0,0,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 1 x 3 matrix,  density 20%, complex
%!test
%! A = [0,0,0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,3) == sparse (A)(1,3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,0,0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,0,0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,0,0,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 1 matrix,  density 20%, real
%!test
%! A = [0,;0,;68,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)(3,1) == sparse (A)(3,1));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;68,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;68,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,;0,;68,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 1 matrix,  density 20%, complex
%!test
%! A = [0,;0,;0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3,1) == sparse (A)(3,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;0,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,;0,;0,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 3 matrix,  density 20%, real
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)(3,3) == sparse (A)(3,3));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)(9) == sparse (A)(9));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (reshape (sparsersb (A), 1, 9) == reshape (sparse (A), 1, 9));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (reshape (sparsersb (A), 3, 3) == reshape (sparse (A), 3, 3));
%!test
%! A = [0,0,0,;3,0,0,;0,0,42,;]; assert (reshape (sparsersb (A), 9, 1) == reshape (sparse (A), 9, 1));

%% tests for a 3 x 3 matrix,  density 20%, complex
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)(3,3) == sparse (A)(3,3));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)(9) == sparse (A)(9));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (reshape (sparsersb (A), 1, 9) == reshape (sparse (A), 1, 9));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (reshape (sparsersb (A), 3, 3) == reshape (sparse (A), 3, 3));
%!test
%! A = [0+20*i,0,0,;51,0,0,;0,0,0,;]; assert (reshape (sparsersb (A), 9, 1) == reshape (sparse (A), 9, 1));

%% tests for a 1 x 1 matrix,  density 50%, real
%!test
%! A = [54,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [54,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [54,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [54,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [54,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [54,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [54,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [54,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [54,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [54,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [54,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [54,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [54,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [54,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [54,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [54,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [54,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [54,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [54,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [54,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [54,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [54,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [54,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [54,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [54,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [54,;]; assert (reshape (sparsersb (A), 1, 1) == reshape (sparse (A), 1, 1));

%% tests for a 1 x 1 matrix,  density 50%, complex
%!test
%! A = [0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;]; assert (reshape (sparsersb (A), 1, 1) == reshape (sparse (A), 1, 1));

%% tests for a 1 x 3 matrix,  density 50%, real
%!test
%! A = [81,0,1,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)(1,3) == sparse (A)(1,3));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [81,0,1,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [81,0,1,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [81,0,1,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [81,0,1,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [81,0,1,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 1 x 3 matrix,  density 50%, complex
%!test
%! A = [0,0+16*i,70,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)(1,3) == sparse (A)(1,3));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,0+16*i,70,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,0+16*i,70,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,0+16*i,70,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 1 matrix,  density 50%, real
%!test
%! A = [0,;10,;9,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)(3,1) == sparse (A)(3,1));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;10,;9,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;10,;9,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,;10,;9,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 1 matrix,  density 50%, complex
%!test
%! A = [0,;0,;60+73*i,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)(3,1) == sparse (A)(3,1));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0,;0,;60+73*i,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0,;0,;60+73*i,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0,;0,;60+73*i,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 3 matrix,  density 50%, real
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)(3,3) == sparse (A)(3,3));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)(9) == sparse (A)(9));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (reshape (sparsersb (A), 1, 9) == reshape (sparse (A), 1, 9));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (reshape (sparsersb (A), 3, 3) == reshape (sparse (A), 3, 3));
%!test
%! A = [58,0,70,;5,62,0,;0,0,86,;]; assert (reshape (sparsersb (A), 9, 1) == reshape (sparse (A), 9, 1));

%% tests for a 3 x 3 matrix,  density 50%, complex
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)(3,3) == sparse (A)(3,3));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)(9) == sparse (A)(9));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (reshape (sparsersb (A), 1, 9) == reshape (sparse (A), 1, 9));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (reshape (sparsersb (A), 3, 3) == reshape (sparse (A), 3, 3));
%!test
%! A = [0+64*i,23+36*i,0,;0,0,0,;8,0,0,;]; assert (reshape (sparsersb (A), 9, 1) == reshape (sparse (A), 9, 1));

%% tests for a 1 x 1 matrix,  density 100%, real
%!test
%! A = [21,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [21,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [21,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [21,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [21,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [21,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [21,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [21,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [21,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [21,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [21,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [21,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [21,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [21,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [21,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [21,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [21,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [21,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [21,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [21,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [21,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [21,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [21,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [21,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [21,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [21,;]; assert (reshape (sparsersb (A), 1, 1) == reshape (sparse (A), 1, 1));

%% tests for a 1 x 1 matrix,  density 100%, complex
%!test
%! A = [94+61*i,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [94+61*i,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [94+61*i,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [94+61*i,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [94+61*i,;]; assert (reshape (sparsersb (A), 1, 1) == reshape (sparse (A), 1, 1));

%% tests for a 1 x 3 matrix,  density 100%, real
%!test
%! A = [38,64,99,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)(1,3) == sparse (A)(1,3));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [38,64,99,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [38,64,99,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [38,64,99,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [38,64,99,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [38,64,99,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 1 x 3 matrix,  density 100%, complex
%!test
%! A = [0+32*i,84+27*i,78,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)(1,3) == sparse (A)(1,3));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)(1,:) == sparse (A)(1,:));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0+32*i,84+27*i,78,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 1 matrix,  density 100%, real
%!test
%! A = [31,;40,;66,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)(3,1) == sparse (A)(3,1));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [31,;40,;66,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [31,;40,;66,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [31,;40,;66,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 1 matrix,  density 100%, complex
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)(3) == sparse (A)(3));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)(3,1) == sparse (A)(3,1));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)(:,1) == sparse (A)(:,1));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (reshape (sparsersb (A), 1, 3) == reshape (sparse (A), 1, 3));
%!test
%! A = [0+90*i,;25+58*i,;26,;]; assert (reshape (sparsersb (A), 3, 1) == reshape (sparse (A), 3, 1));

%% tests for a 3 x 3 matrix,  density 100%, real
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)(3,3) == sparse (A)(3,3));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)(9) == sparse (A)(9));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (reshape (sparsersb (A), 1, 9) == reshape (sparse (A), 1, 9));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (reshape (sparsersb (A), 3, 3) == reshape (sparse (A), 3, 3));
%!test
%! A = [86,42,100,;1,53,72,;6,97,38,;]; assert (reshape (sparsersb (A), 9, 1) == reshape (sparse (A), 9, 1));

%% tests for a 3 x 3 matrix,  density 100%, complex
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert ( (sparsersb (A)) ==  (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (istril (sparsersb (A)) == istril (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (istriu (sparsersb (A)) == istriu (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (isreal (sparsersb (A)) == isreal (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (iscomplex (sparsersb (A)) == iscomplex (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (issymmetric (sparsersb (A)) == issymmetric (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (ishermitian (sparsersb (A)) == ishermitian (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (nnz (sparsersb (A)) == nnz (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (rows (sparsersb (A)) == rows (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (columns (sparsersb (A)) == columns (sparse (A)));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)'*(1*ones(size(A,1))) == sparse (A)'*(1*ones(size(A,1))));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)'*(i*ones(size(A,1))) == sparse (A)'*(i*ones(size(A,1))));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)( ) == sparse (A)( ));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)(1) == sparse (A)(1));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)(1,1) == sparse (A)(1,1));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)(3,3) == sparse (A)(3,3));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)(3,:) == sparse (A)(3,:));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)(9) == sparse (A)(9));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)(:) == sparse (A)(:));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)(:,3) == sparse (A)(:,3));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)(:,:) == sparse (A)(:,:));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)*(1*ones(size(A,2))) == sparse (A)*(1*ones(size(A,2))));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)*(i*ones(size(A,2))) == sparse (A)*(i*ones(size(A,2))));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)*1 == sparse (A)*1);
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A)*i == sparse (A)*i);
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A).'*(1*ones(size(A,1))) == sparse (A).'*(1*ones(size(A,1))));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (sparsersb (A).'*(i*ones(size(A,1))) == sparse (A).'*(i*ones(size(A,1))));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (reshape (sparsersb (A), 1, 9) == reshape (sparse (A), 1, 9));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (reshape (sparsersb (A), 3, 3) == reshape (sparse (A), 3, 3));
%!test
%! A = [88,95,43,;0+76*i,26+64*i,0,;0+61*i,45+54*i,0+15*i,;]; assert (reshape (sparsersb (A), 9, 1) == reshape (sparse (A), 9, 1));
%% Generated with rand state 42 .
*/ /* GENERATED TEST LINES END */

/*
%% tests for different operators multiplications

%!test % REAL/REAL
%! A = sprandn(1e3,1e3,1/1e3); Arsb = sparsersb(A); v = randn(1e3,1);
%! assert(A*v,Arsb*v,1e-14);
%! assert(A.'*v,Arsb.'*v,1e-14);
%! assert(A'*v,Arsb'*v,1e-14);

%!test % REAL/COMPLEX
%! A = sprandn(1e6,1e6,1/1e6); Arsb = sparsersb(A); v = 1i*randn(1e6,1) + randn(1e6,1);
%! assert(A*v,Arsb*v,1e-14);
%! assert(A.'*v,Arsb.'*v,1e-14);
%! assert(A'*v,Arsb'*v,1e-14);

%!test % COMPLEX/REAL
%! A = 1i*sprandn(1e3,1e3,1/1e3) + sprandn(1e3,1e3,1/1e3); Arsb = sparsersb(A); v = randn(1e3,1);
%! assert(A*v,Arsb*v,1e-14);
%! assert(A.'*v,Arsb.'*v,1e-14);
%! assert(A'*v,Arsb'*v,1e-14);

%!test % COMPLEX/COMPLEX
%! A = 1i*sprandn(1e3,1e3,1/1e3) + sprandn(1e3,1e3,1/1e3); Arsb = sparsersb(A); v = 1i*randn(1e3,1) + randn(1e3,1);
%! assert(A*v,Arsb*v,1e-14);
%! assert(A.'*v,Arsb.'*v,1e-14);
%! assert(A'*v,Arsb'*v,1e-14);

*/

/*
%!demo
%! disp("'sparsersb' behaves pretty like 'sparse':")
%! R=(rand(3)>.6)
%! A_octave=sparse(R)
%! A_librsb=sparsersb(R)

%!demo
%! disp("The interface of 'sparsersb' is almost like the one of 'sparse'")
%! disp("Create a 1x1 matrix:")
%! sparsersb([2])
%! disp("Create a 2x1 matrix:")
%! sparsersb([1,2],[1,1],[11,21])
%! disp("Create a 2x2 matrix:")
%! sparsersb([1,2],[1,1],[11,21],2,2)
%! disp("Create a 2x2 lower triangular matrix:")
%! sparsersb([1,2,2  ],[1,1,2  ],[11,21,   22],2,2)

%!demo
%! disp("'sparsersb' has an option to handle duplicates.")
%! disp("Create a 2x2 lower triangular matrix (last element summed by default):")
%! sparsersb([1,2,2,2],[1,1,2,2],[11,21,11,11],2,2)
%! disp("Create a 2x2 lower triangular matrix (last two elements summed explicitly):")
%! sparsersb([1,2,2,2],[1,1,2,2],[11,21,11,11],2,2,"sum")
%! disp("Create a 2x2 lower triangular matrix (last element ignored, explicitly):")
%! sparsersb([1,2,2,2],[1,1,2,2],[11,21,11,11],2,2,"unique")

%!demo
%! disp("'sparsersb' support symmetric and hermitian matrices:\n")
%! disp("2x2 lower tringular:")
%! sparsersb([1,2,2  ],[1,1,2  ],[11,21 ,  22],2,2,"general")
%! disp("2x2 symmetric (only lower triangle stored):")
%! sparsersb([1,2,2  ],[1,1,2  ],[11,21 ,  22],2,2,"symmetric")
%! disp("2x2 hermitian (only lower triangle stored):")
%! sparsersb([1,2,2  ],[1,1,2  ],[11,21i,  22],2,2,"hermitian")

%!demo
%! disp("Any 'sparse' or 'dense' matrix can be converted to 'sparsersb':")
%! d=sparsersb(       [1,2;3,4] )
%! f=sparsersb( full ([1,2;3,4]))
%! s=sparsersb(sparse([1,2;3,4]))

%!demo
%! disp("'sparsersb' detects symmetry:")
%! d=sparsersb(       [1,2;2,1] )
%! s=sparsersb(sparse([1,2;2,1]))

%!demo
%! disp("'sparsersb' detects hermitianness:")
%! d=sparsersb(       [1,i;-i,1] )
%! s=sparsersb(sparse([1,i;-i,1]))

%!demo
%! disp("The most important use of 'sparsersb' is for multiplying sparse matrices...\n")
%! a=sparsersb(       [1,2;3,4] )
%! disp("...by dense matrices or vectors:\n")
%! x=[1,2;1,2]
%!
%! disp("Untransposed sparse matrix-vector multiplication:")
%! a*x
%!
%! disp("Transposed sparse matrix-vector multiplication:")
%! a'*x

%!demo
%! d=sparsersb(       [1,2;3,4] );
%! s=sparsersb(sparse([1,2;3,4]));
%!
%! disp("Many sparse-sparse matrix operators work on 'sparsersb'\n")
%! disp("'+' operator:")
%! s+d
%! disp("'.+' operator:")
%! s.+d
%! disp("'-' operator:")
%! s-d
%! disp("'.-' operator:")
%! s.-d
%! disp("'*' operator:")
%! s*d
%! disp("'.*' operator:")
%! s.*d
%! disp("'/' operator:")
%! s/d
%! disp("'./' operator:")
%! s./d
%! disp("'\\' operator:")
%! s\[1;1]
%! disp("And others. Not all operators are native: certain use a conversion; see the printout.\n")

%!demo
%! o=sparse(   [1,2;3,4] );
%! s=sparsersb([1,2;3,4] );
%!
%! disp("Most of these operators hide a conversion; see the printout:\n")
%!
%! s(:,:)
%! o(:,:)
%!
%! s(:,2)
%! o(:,2)
%!
%! s(2,:)
%! o(2,:)
%!
%! s(:)
%! o(:)

%!demo
%! disp("On large matrices 'sparsersb' may be faster than 'sparse' in sparse matrix-vector multiplication.")
%! disp("In addition to that, 'sparsersb' has an 'empirical online auto-tuning' functionality.")
%! disp("It means you run the autotuning on a specific input, and just after, the multiplication might be faster.")
%! disp("See this case with two different right hand sides (NRHS) count.\n")
%! M=100000;
%! N=100000;
%! P=100 / M;
%! s=sparse(sprand(M,N,P));
%!
%! for NRHSc = {1,7}
%! r=sparsersb(s); # repeat tuning from 'vanilla' matrix
%! assert(nnz(s)==nnz(r))
%! NRHS=cell2mat(NRHSc);
%!
%! x=ones(M,NRHS);
%! printf("Here, a %.2e x %.2e matrix with %.2e nonzeroes, %d NRHS.\n",M,N,nnz(s),NRHS)
%! tic();
%! sc=0;
%! while(toc()<3)
%!   s*x;
%!   sc=sc+1;
%! endwhile
%! st=toc()/sc;
%! printf("Each multiplication with 'sparse' took %.1es.\n",st);
%!
%! tic();
%! rc=0;
%! while(toc()<3)
%!   r*x;
%!   rc=rc+1;
%! endwhile
%! rt=toc()/rc;
%! ut=rt; # untuned time
%! printf("Each multiplication with 'sparsersb' took %.3es, this is %.4g%% of the time taken by 'sparse'.\n",rt,100*rt/st);
%!
%! nsb=str2num(sparsersb(r,"get","RSB_MIF_LEAVES_COUNT__TO__RSB_BLK_INDEX_T"));
%! tic;
%! r=sparsersb(r,"autotune","n",NRHS);
%! at_t=toc;
%! nnb=str2num(sparsersb(r,"get","RSB_MIF_LEAVES_COUNT__TO__RSB_BLK_INDEX_T"));
%! printf ("Autotuning for %d NRHS took  %.2es (%d -> %d RSB blocks).\n", NRHS, at_t, nsb, nnb);
%! tic();
%! rc=0;
%! while(toc()<3)
%!   r*x;
%!   rc=rc+1;
%! endwhile
%! rt=toc()/rc;
%! printf("After tuning, each 'sparsersb' multiplication took %.3es.\n",rt);
%! printf("This is %.4g%% of the time taken by 'sparse' (%.2fx speedup).\n",100*rt/st,st/rt);
%! if ut > rt;
%!   printf ("Autotuning brought a %.2fx speedup over original RSB structure.\n", ut/rt);
%!   printf ("Time spent in autotuning can be amortized in %.1d iterations.\n", at_t/(ut-rt) );
%! else
%!   printf ("RSB autotuning brought no further speedup for NRHS=%d.\n",NRHS);
%! endif
%! disp("")
%! endfor

%!demo
%! disp("'sparsersb' can render sparse matrices into Encapsulated Postscript files showing the RSB blocks layout.")
%! rm = sparsersb(sprand(100000,100000,.0001));
%! sparsersb(rm,'render','sptest.eps')
%! disp("You can open sptest.eps now.")
%%!demo
*/
